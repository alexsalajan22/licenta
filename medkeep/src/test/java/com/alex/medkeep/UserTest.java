package com.alex.medkeep;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import com.alex.medkeep.model.*;
import com.alex.medkeep.repository.DepartmentRepository;
import com.alex.medkeep.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserTest {

    static DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    public Date date = new Date(2018, 5, 3);
    @Autowired
    private UserRepository userRepo;
    @Autowired
    private DepartmentRepository departmentRepo;

    @Test
    public void addPatient() {

        Patient patient = new Patient("Andru", "Jan", date,
                "Mail", "0743",
                "195", "sm", "sm");

        patient = userRepo.save(patient);
        User user1 = userRepo.findOne(patient.getIdUser());

        assertEquals(patient.getIdUser(), user1.getIdUser());

        userRepo.delete(patient);
    }

    @Test
    public void addMedic() {

        Department department = new Department("Oftamologie", "Ochi");
        department = departmentRepo.save(department);

        Medic medic = new Medic("Alex", "Salajan", date, "email", "0231",
                "password", "Copii", "Medicul", department);

        medic = userRepo.save(medic);
        User user2 = userRepo.findOne(medic.getIdUser());

        assertEquals(medic.getIdUser(), user2.getIdUser());

        userRepo.delete(medic);
        departmentRepo.delete(department);
    }

    @Test
    public void addReceptionist() {

        Receptionist receptionist = new Receptionist("Andru", "Jan", date, "Mail", "0743",
                "195", "sm");

        receptionist = userRepo.save(receptionist);
        User user1 = userRepo.findOne(receptionist.getIdUser());

        assertEquals(receptionist.getIdUser(), user1.getIdUser());

        userRepo.delete(receptionist);
    }

    @Test
    public void addAdministrator() {

        Date date = new Date(2018, 5, 3);

        Administrator administrator = new Administrator("Andru", "Jan", date, "Mail", "0743",
                "password");

        administrator = userRepo.save(administrator);
        User user1 = userRepo.findOne(administrator.getIdUser());

        assertEquals(administrator.getIdUser(), user1.getIdUser());

        userRepo.delete(administrator);
    }

    @Test
    public void deleteUser() {

        Patient patient = new Patient("Andru", "Jan", date, "Mail", "0743",
                "195", "sm", "sm");

        patient = userRepo.save(patient);
        userRepo.delete(patient);

        assertNull(userRepo.findOne(patient.getIdUser()));

    }

    @Test
    public void updateUser() {

        Department department = new Department("Oftamologie", "Ochi");
        department = departmentRepo.save(department);

        Medic medic = new Medic("Alex", "Salajan", date, "email", "0231",
                "password", "Copii", "Medicul", department);

        medic = userRepo.save(medic);
        medic.setSpecialization("newSpec");
        medic = userRepo.save(medic);
        Medic dbMedic = (Medic) userRepo.findOne(medic.getIdUser());

        assertEquals("newSpec", dbMedic.getSpecialization());

        userRepo.delete(medic.getIdUser());
        departmentRepo.delete(department.getIdDepartment());
    }


    @Test
    public void findUserById() {

        Patient patient = new Patient("Andru", "Jan", date, "Mail", "0743",
                "195", "sm", "sm");

        patient = userRepo.save(patient);
        Patient dbPatient = (Patient) userRepo.findOne(patient.getIdUser());

        assertNotNull(dbPatient);

        userRepo.delete(patient);
    }

    @Test
    public void getAllUsers() {

        Patient patient = new Patient("Andru", "Jan", date, "Mail", "0743",
                "195", "sm", "sm");
        Patient patient2 = new Patient("Alx", "Man", date, "hemail", "0343",
                "19523", "cj", "cj");

        patient = userRepo.save(patient);
        patient2 = userRepo.save(patient2);
        List<User> userList = userRepo.findAll();

        assertNotNull(userList);
        assertEquals(2, userList.size());

        userRepo.delete(patient);
        userRepo.delete(patient2);
    }

    @Test
    public void findAllByUserType() {

        Patient patient = new Patient("Andru", "Jan", date, "Mail", "0743",
                "195", "sm", "sm");
        Patient patient2 = new Patient("Alx", "Man", date, "hemail", "0343",
                "19523", "cj", "cj");

        patient = userRepo.save(patient);
        patient2 = userRepo.save(patient2);
        List<User> patientList = userRepo.findAllByUserType(User.UserType.PATIENT);
        List<User> medicList = userRepo.findAllByUserType(User.UserType.MEDIC);

        assertNotNull(patientList);
        assertEquals(2, patientList.size());
        assertEquals(0, medicList.size());

        userRepo.delete(patient);
        userRepo.delete(patient2);
    }

    @Test
    public void findAllByFirstNameAndLastName() {

        Patient patient = new Patient("Andru", "Jan", date, "Mail", "0743",
                "195", "sm", "sm");

        patient = userRepo.save(patient);
        List<User> patientList = userRepo.findAllByFirstNameAndLastName("Andru", "Jan");

        assertEquals(1, patientList.size());

        userRepo.delete(patient);
    }

    @Test
    public void findByEmail() {

        Patient patient = new Patient("Andru", "Jan", date, "Mail", "0743",
                "195", "sm", "sm");

        patient = userRepo.save(patient);
        Patient dbPatient = (Patient) userRepo.findByEmail("Mail");


        assertEquals("Mail", dbPatient.getEmail());

        userRepo.delete(patient);

    }

    @Test
    public void findAllMedicsByDepartment() {

        Department department = new Department("Oftamologie", "Ochi");
        department = departmentRepo.save(department);

        Medic medic = new Medic("Alex", "Salajan", date, "email", "0231",
                "password", "Copii", "Medicul", department);
        Medic medic2 = new Medic("Andru", "Janjan", date, "hemail", "02231",
                "psswd", "Coptiii", "Medicull", department);

        medic = userRepo.save(medic);
        medic2 = userRepo.save(medic2);
        department = departmentRepo.save(department);
        Department dbDepartment = departmentRepo.findOne(department.getIdDepartment());
        List<Medic> medicList = userRepo.findAllMedicsByDepartment(dbDepartment);

        assertEquals(2, medicList.size());

        userRepo.delete(medic);
        userRepo.delete(medic2);
        departmentRepo.delete(department);

    }

//    @Test
//    public void justAddMedic() {
//
//        Department department = new Department("Oftamologie", "Ochi");
//        department = departmentRepo.save(department);
//
//        Medic medic = new Medic("Alex", "Salajan", date, "email", "0231",
//                "password", "Copii", "Medicul", department);
//
//        medic = userRepo.save(medic);
//        User user2 = userRepo.findOne(medic.getIdUser());
//
//        assertEquals(medic.getIdUser(), user2.getIdUser());
//
//        //userRepo.delete(medic);
//        departmentRepo.delete(department);
//    }


}

