package com.alex.medkeep;

import com.alex.medkeep.model.*;
import com.alex.medkeep.repository.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DataFiller {

    @Autowired
    private ConsultationRepository consultationRepository;

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private DiagnosticRepository diagnosticRepository;

    @Autowired
    private ServiceRepository serviceRepository;

    @Autowired
    private TreatmentRepository treatmentRepository;

    @Autowired
    private UserRepository userRepository;


    @Test
    public void fillData() {

        Treatment t1 = new Treatment("Tratament naturist pentru ochii obositi", "-\tInmoaie doua tampoane de vata intr-un amestec format din apa de trandafiri cu 2-3 picaturi de ulei de ricin. Aseaza aceste tampoane pe ochi si relaxeaza-te 15-20 de minute. Arsurile oculare si alte probleme specifice ochiului vor fi ameliorate.\n" +
                "-\tCompresele cu ceai verde sau negru, aplicate calde inainte de culcare vor reda tonusul ochilor tai.Cafeina din aceste ceaiuri stimuleaza circulatia sanguina si reduce inflamatiile.\n" +
                "-\tCartoful proaspat, taiat felii si asezat pe ploape face minuni. Stai cu feliile de cartofi cel putin 10 minute si vei vedea rezultatele. Cartofii au proprietati antiinflamatorii si sunt remediile perfecte pentru ochii iritati.\n" +
                "-\tCastravetele are proprietati astringente. Feliile de castravete se aplica pe ploape si se lasa acolo cel putin 10 minute.",
                500, "Oftalmologie");

        Treatment t2 = new Treatment("Tratament pentru ochii umflati", "O dieta dominata de alimente sarate, alergiile si sinuzitele cornice sunt cele mai comune cauze ale umflarii ochilor. De asemenea, substantele chimice din produsele cosmetice sau detergenti pot avea efecte inflamatorii asupra ochilor. Umflarea ochilor este o problema temporara si poate fi tratata acasa cu remedii naturiste cum ar fi, gheata, compresele cu ceai verde rece, comprese cu lapte nefiert si rece, comprese cu apa imbogatita cu vitamina E, lingura rece aplicata in zona ochilor si felii de castravete crud aplicate pe pleoape. Relaxeaza-te 10-15 minute cu un astfel de tratament aplicat local si ochii tai vor arata mult mai bine.",
                700, "Oftalmologie");
        Treatment t3 = new Treatment("Tratament pentru cearcane", "Castravetele este ingredientul \"minune\" folosit in combaterea cercanelor.In diferite combinatii acesta lumineaza zonele de sub ochi si reduce senzatia de oboseala a ochilor. Iata cateva \"retete\":\n" +
                "-Sucul de castravete (sau o felie de castravete) poate fi aplicat zilnic in zonele de sub ochi. Asteapta 15 minute si apoi spala fata cu apa calduta.\n" +
                "-Sucul de castravete amestecat cu suc de cartof, in parti egale, ajuta in cazul \"pungilor\" de sub ochi. Urmeaza aceeasi procedura de mai sus.\n" +
                "- Cearcanele pot fi, de asemenea, tratate cu suc de castravele si suc de lamaie, in parti egale. Aplica pe zonele intunecate de sub ochi, lasa 15 minute si apoi clateste.\n" +
                "- Sucul de rosii ajuta si el la recapatarea stralucirii in zonele de sub ochi. Se aplica la fel ca celelalte amestecuri.",
                950, "Oftalmologie");
        Treatment t4 = new Treatment("Tratamentul radacinilor dentare", "Tratamentul de canal se aplica in cazul in care nervul dintelui este distrus ireversibil de carie sau de o trauma. In acest caz pulpa dentara se poate inflama si se mareste in volum, iar din cauza presiunii pe care aceasta o exercita asupra cavitatii pulpare, apare senzatia de durere. Simptomele care apar si care indica necesitatea unui tratament de canal sunt sensibilitatea dintelui in cazul consumului de alimente prea reci sau fierbinti, durere in cazul in care mestecam pe dintele respectiv, coloratie a dintelui sau durere constanta.\n" +
                "\n" +
                "Nu intotdeauna insa acest tratament se aplica unui dinte bolnav. In cazul in care se efectueaza lucrari protetice in care dintii invecinati folosesc ca suport si vor fi slefuiti, tratamentul de canal poate fi facut preventiv in cazul acestora pentru a evita distrugerea acestor lucrari in cazul unor infectii ulterioare care pot sa apara.\n"
                ,2300, "Stomatologie");

        Treatment t5 = new Treatment("Albire Dentara", "Coloratiile afecteaza culoarea smaltului datorita consumului de cafea, tutun, coloranti alimentari, dar si din cauza tratamentului cu medicamente de tip tetraciclina, acesta din urma ducand la coloratii severe. \n" +
                "\n" +
                "Albirea dintilor cu acceleratorul de albire Beyond este recomadata persoanelor consumatoare de cafea, tutun, sucuri sau alte alimente care in timp pot modifica culoarea dintilor.\n" +
                "\n" +
                "Cu puterea noului Accelerator de albire Beyond prezent in clinica noastra din Cluj-Napoca puteti scapa aceste coloratii deoarece acesta permite atingerea a pana la 12 nuante pe cheia VITA a culorii smaltului. \n" +
                "\n" +
                "Efectul albirii dintilor are perioada de viata de pana la 5 ani in functie de obiceiurile dumneavoastra alimentare, precum si de stilul dvs. de viata. \n",
                1350, "Stomatologie");
        Treatment t6 = new Treatment("Tratament migrene", " Procedura de kinetoterapie, folosind ca mijloc de baza exercitiul fizic este indicata in tratarea migrenelor. Exercitiile fizice imbunatatesc starea emotionala si mentala, au un important efect relaxant si imbunatatesc in general starea fizica. Exercitiile fizice, practicate in mod regulat, pot ajuta de asemenea in prevenirea migrenelor. Facand exercitii fizice, atunci cand durerea datorata migrenei este minora, se pot elimina mult mai usor aceste dureri.  Anumite tehnici ale terapiei prin miscare, sunt in masura sa contribuie la eliminarea migrenelor. Se considera ca o migrena clasica este adesea insotita de tensiune la nivelul gatului. Insa medicii sunt de parere ca tensiunea este rezultatul migrenei si nu cauza acesteia. Reducand tensiunea de la nivelul gatului prin exercitii de stretching, se poate reduce durerea cat si disconfortul adus de migrena. ",
                845, "Medicina Generala");
        Treatment t7 = new Treatment("Tratamente artroza", "Procedura de electroterapie prezinta un rol extrem de favorabil in tratarea artrozei. In scopul calmarii durerilor cauzate de artroza, electroterapia vine in ajutorul pacientului prin formele de curent antialgice, miorelaxante, decontracturante si antiiflamatorii, pregatind totodata pacientul pentru tratarea afectiunii prin kinetoterapie. Electroterapia, constituita din: curenti interferentiali, curent galvanic, ultrasunete, radiatii infrarosii, are urmatoarele efecte benefice, pe langa cel de ameliorare a durerilor, in tratarea artrozei: reduce inflamatia, stimuleaza musculatura din jurul articulatiei afectate, creste mobilitatea articulara.",
                1460, "Medicina Generala");

        treatmentRepository.save(t1);
        treatmentRepository.save(t2);
        treatmentRepository.save(t3);
        treatmentRepository.save(t4);
        treatmentRepository.save(t5);
        treatmentRepository.save(t6);
        treatmentRepository.save(t7);


        Department d1 = new Department("Medicina Generala", "Medicina generala este specialitatea care se ocupa cu diagnosticarea si tratamentul conditiilor acute sau cronice si furnizarea de ingrijiri preventive de sanatate. Specialistii de medicina generala / medicina de familie furnizeza ingrijiri medicale pentru pacientii de toate varstele, de la nou-nascuti la varstnici, barbati si femei, cu probleme medicale diverse.");
        Department d2 = new Department("Stomatologie", "Stomatologia este știința care se ocupă cu studiul formațiunilor anatomice și cu tratarea bolilor care apar la nivelul cavității orale. Formațiunile anatomice care fac obiectul stomatologiei sunt grupate sub numele de aparat dento-maxilar.");
        Department d3 = new Department("Oftalmologie", "Oftalmologia este o ramură a medicinei care se ocupă cu tratarea bolilor organului vederii, ceea ce include ochiul și structurile din jurul ochiului, cum ar fi pleoapele și sistemul lacrimar. Patologia căilor vizuale (nervul optic, căile vizuale centrale) și a cortexului vizual reprezintă un domeniu de graniță între neurologie și oftalmologie. Cuvântul oftalmologie vine din radicalurile grecești ophthalmos, care înseamnă ochi și logos, care înseamnă cuvânt, gând sau discurs; oftalmologia înseamnă, literal, \"știința despre ochi\". ");

        departmentRepository.save(d1);
        departmentRepository.save(d2);
        departmentRepository.save(d3);

        Patient p1 = new Patient("User", "Pacient", new Date(73,2,19), "pacient@yahoo.com", "09471237452", "1234", "Str. Paris nr 23", "Cluj Napoca");
        Patient p2 = new Patient("Marian", "Cercel", new Date(90,9,14), "marian@gmail.com", "0754712323", "19602039123", "Str. Marcelobivi nr 3", "Cluj Napoca");
        Patient p3 = new Patient("Marcel", "Pavel", new Date(73,5,23), "marcep@yahoo.com", "09471237452", "173723823583", "Str. Paris nr 5", "Cluj Napoca");
        Patient p4 = new Patient("Mihai", "paraschiv", new Date(86,3,2), "mihupar@yahoo.com", "09471237452", "193238125382", "Str. Mures nr 13", "Cluj Napoca");
        Patient p5 = new Patient("Florin", "Anghel", new Date(79,8,12), "florinel@yahoo.com", "075823912", "1786764283817", "Str. Avram Iancu nr 5", "Cluj Napoca");

        userRepository.save(p1);
        userRepository.save(p2);
        userRepository.save(p3);
        userRepository.save(p4);
        userRepository.save(p5);

        Medic m1 = new Medic("Doctor", "Medic", new Date(70,11,23), "medic@yahoo.com", "073823123", "1234", "Viroze respiratorii", "Specializat din anul 1988 pe diferitele tipuri de viroze respiratorii", d1 );
        Medic m2 = new Medic("Florin", "Chis", new Date(76,1,1), "florin@yahoo.com", "073823123", "florin", "Analize generale, Alegenici", "Specializat pe interpretarea analizelor si a rezultatelor de mai bine de 20 ani", d1 );
        Medic m3 = new Medic("Maria", "Elenora", new Date(89,11,23), "maria@yahoo.com", "073823123", "florin", "Gripa Acuta", "Urmand cursuri in Viena si Berlin, Cunostintele doctorului sunt foarte vaste idiferent de tipul gripei.", d1 );
        Medic m4 = new Medic("Stefan", "Marchis", new Date(65,2,21), "stefan@yahoo.com", "073823123", "florin", "Afectiuni pulmonare", "Membru al academiei din 2011, Specializat pe un numar mare de afectiuni pulmonare", d1 );

        Medic m5 = new Medic("Dica", "Malina", new Date(67,10,23), "dica@yahoo.com", "073823123", "florin", "Dentara Generala", "Specializat din anul 1995 pe implanturi si plombe, peste 20 ani de experienta cu rata ridicata de succes", d2 );
        Medic m6 = new Medic("Marin", "Vid", new Date(87,3,23), "marin@yahoo.com", "073823123", "florin", "Estetica Dentara", "Estetica dentara isi propune refacerea zambetului pacientului insa gasirea unui medic calificat, specialist in estetica dentara, necesita atentie deosebita.", d2 );
        Medic m7 = new Medic("Popescu", "Ionut", new Date(78,8,2), "popescu@yahoo.com", "073823123", "florin", "Implantologie", "Implantologia este o ramură a stomatologiei ce rezolvă problemele de edentație (lipsa dinților) cu ajutorul implanturilor dentare.", d2 );

        Medic m8 = new Medic("Gruia", "Mirela", new Date(76,6,23), "gruia@yahoo.com", "073823123", "florin", "Oftamologie generala", "Specializat din anul 2004 pe diferitele tipuri de boli oculare si afectiuni vizuale", d3 );
        Medic m9 = new Medic("Mihaela", "Ioana", new Date(74,4,27), "mihaela@yahoo.com", "073823123", "florin", "Operatii lase, schimb cristalin", "Cu ajutorul acestei metode, lentila din interiorul ochiului, ce poarta numele de cristalin, este inlocuita cu un cristalin artiﬁcial multifocal cu o dioptrie adaptata nevoilor ochiului respectiv, in asa fel incat, dupa operatie, sa nu mai aiba nevoie de ochelari.", d3 );


        userRepository.save(m1);
        userRepository.save(m2);
        userRepository.save(m3);
        userRepository.save(m4);
        userRepository.save(m5);
        userRepository.save(m6);
        userRepository.save(m7);
        userRepository.save(m8);
        userRepository.save(m9);

        Service s1 = new Service("Consultatie generala", "La Medkeep am creat pachete de control total pentru o scanare rapidă a stării de sănătate în funcţie de vârstă, gen, afecţiuni. Pacientul trece într-o singură zi toate etapele de control specifice fiecărui tip de checkup, pentru ca la final să primească concluzia finală şi o evaluare a stării sale de sănătate. ",
                100,  d1);
        Service s2 = new Service("Analize de sange", "Analize complete de sange pentru a verifica starea snatatii generale sau pentru a determina anumite boli, in funtie de nevoi",
                150,  d1);
        Service s3 = new Service("EKG standard", "Inima produce mici curenți electrici care se distribuie în tot mușchiul cardiac făcând-o să bată, adică să se contracte. Electrocardiograma (EKG) înregistrează această activitate electrică a inimii, afișând modul în care circulă curenții electrici prin mușchi. În acest fel, EKG-ul dă medicului foarte ușor indicii asupra modului în care se contractă inima, ritmicitatea, frecvența și normalitatea bătăilor acesteia.",
                130,  d1);
        Service s4 = new Service("Teste cutanate", "In timpul unui test cutanat alergic, pielea este expusa la o substanta care determina alergie (alergen) si apoi se urmaresc semnele reactiei alergice locale. Impreuna cu istoricul medical al pacientului, testele la alergeni pot confirma daca anumite semne si simptome cum ar fi stranutul, respiratia suieratoare si eruptiile cutanate sunt determinate de alergii. \n" + "\n" + "Testele la alergeni pot de asemeni identifica substantele specifice care pot declansa reactiile alergice. Informatiile obtinute in urma acestor teste pot ajuta medicul sa dezvolte un plan de tratament al alergiei care poate include evitarea alergenului, medicatie sau imunoterapie.",
                200,  d1);
        Service s5 = new Service("Testare cutantă alergologică patch", "Test cutanat special utilizant un patch special de testare. estele la alergeni pot de asemeni identifica substantele specifice care pot declansa reactiile alergice. Informatiile obtinute in urma acestor teste pot ajuta medicul sa dezvolte un plan de tratament al alergiei care poate include evitarea alergenului, medicatie sau imunoterapie.",
                250,  d1);
        Service s6 = new Service("Audiograma", "Audiograma tonala liminara este procedeul de baza, devenind procedura comportamentala standard pentru a descrie sensibilitatea auditiva. Comparand pragurile din conducerea osoasa cu cele din conducerea aeriana furnizeaza informatii fundamentale despre functia auditiva, monitorizeaza sensibilitatea auditiva pentru a detecta schimbarile intervenite in timp. In urma acestui test de auz, vi se vor recomanda aparatele auditive potrivite pentru dumneavoastra.\n" +
                "\n" +
                "Examenul audiometric se realizeaza in medii fara zgomot (cabina insonora) cu ajutorul audiometrului. Inainte de inceperea testelor audiometrice trebuie indeplinite anumite cerinte din partea pacientului si din partea mediului in care se desfasoara testarea.",
                180,  d1);
        Service s7 = new Service("examinarea ORL cu mijloace optice", "O examinare ORL cu ajutorul testelor audiometrice este un examen care se realizează de către un medic specialist în otorinolaringologie, al cărui scop este de a diagnostica, monitoriza sau exclude prezența bolilor care afectează urechile, nasul și gâtul. Dincolo de pacienţi cu patologii în curs de desfăşurare, un pacient poate vizita un otorinolaringolog pentru a afla natura simptomelor cu care ei se confruntă, cum ar fi prezența tinitus, pierderea auzului, zgomote, senzație de confuzie, vertij pozițional, dureri persistente în gât, de scădere persistentă a vocii, și nas înfundat fără nici un motiv aparent. Examenul este util pentru diagnosticarea tulburărilor acestor organe, care sunt distincte, dar legate anatomic funcțional între ele, precum și de a stabili tratamentul lor. Acest tip de vizită poate fi de asemenea folosit pentru a monitoriza evoluția posibilă a patologiilor curente și tratamente la pacienți.",
                120,  d1);
        Service s8 = new Service("EKG de efort la persoanele fără risc cardiovascular înalt", "Ekg pentru determinarea rezistentei la efort, pentru persoane fara risc cardiovascular ridicat. Inima produce mici curenți electrici care se distribuie în tot mușchiul cardiac făcând-o să bată, adică să se contracte. Electrocardiograma (EKG) înregistrează această activitate electrică a inimii, afișând modul în care circulă curenții electrici prin mușchi. În acest fel, EKG-ul dă medicului foarte ușor indicii asupra modului în care se contractă inima, ritmicitatea, frecvența și normalitatea bătăilor acesteia.",
                150,  d1);


        Service s9 = new Service("Control stomatologic", "Consultatia stomatologica are ca obiectiv primar formarea unei legaturi de tip medic-pacient si presupune evaluarea starii de sanatate orala stabilindu-se astfel, dupa caz, un diagnostic urmat de un plan de tratament. Cu toate ca pare o banalitate , o atitudine neglijenta in ceea ce priveste consultatia ,din partea doctorului sau a pacientului aduce dupa sine repercusiuni grave cum ar fi un tratament incorect . Tocmai pentru ca conceptual de consultatie are la baza relatia dintre doi oameni, amandoi se fac vinovati pentru eventualele erori survenite datorita neglijarii acestei etape.",
                100,  d2);
        Service s10 = new Service("Detartraj cu ultrasunete + periaj profesional", "Detartrajul este manoperă de îndepărtare a tartrului (pietrei) de pe dinţi. Tartrul nu poate fi îndepărtat cu periuţa de dinţi sau folosirea aţei dentare, ci doar de către medicul stomatolog. Detartrajul se realizeză cu ajutorul aparatelor cu ultasunete astfel încât procedura este rapidă, foarte eficientă şi uşor de suportat de către pacienţi. Tartrul se depune în mod normal pe dinţi astfel încât detartrajul periodic la 4-6 luni este o necesitate care trebuie să între în obişnuinţă fiecăruia dintre noi. Din acest motiv oferim pacienţilor noştri reducere de 50% la efectuarea detartrajului efectuat cu ocazia controalelor periodice la 6 luni.",
                300,  d2);
        Service s11 = new Service("Implant dentar (fără coroană de ceramică)", "Un implant dentar este un şurub de titan care se plasează în os pentru a înlocui rădăcina unui dinte care lipseşte. Peste acest implant din titan se va monta coroana dintelui, astfel încât, la finalul tratamentului, veţi avea un “dinte” care se comportă ca dinţii naturali când mâncaţi, vorbiţi sau râdeți. Pentru plasarea unui implant în os, acesta trebuie să aibă anumite dimensiuni care să-i asigure stabilitatea. Dacă osul nu este suficient (înălţime, grosime) se poate face în prealabil adiţie (adăugare) de os artificial. Alternativă la implant dentar pentru a înlocui un dinte extras este realizarea unei punţi (pod) care să se sprijine pe dinţii vecini care trebuie şlefuiţi (piliţi) chiar dacă sunt perfect sănătoşi şi nu au nevoie de vreun tratament.",
                2100,  d2);


        Service s12 = new Service("Consultatie Oftalmologica", "Fiind dotata cu echipamente ultramoderne, la cel mai inalt nivel tehnologic, Medkeep va pune la dispozitie o gama completa de servicii medicale oftalmologice, de cea mai buna calitate. Consultatiile sunt efectuate de catre medici cu o bogata experienta si pregatire profesionala. În funcție de diagnostic, pacientul va fi orientat către un tratament specific, către optica medicală (pentru fabricarea ochelarilor sau comandarea lentilelor de contact) sau către operație. Toate aceste investigații și tratamente (inclusiv operația) sunt nedureroase, foarte eficiente și nu necesită nici o pregătire specială din partea pacientului.",
                120,  d3);
        Service s13 = new Service("Operatie cataracta", "Cataracta reprezinta opacifierea cristalinului, lentila transparenta din interiorul ochiului, care focalizeaza lumina spre retina pentru obtinerea unei imagini clare.\n" +
                "\n" +
                "Tehnica chirurgicala cel mai frecvent utilizata, numita facoemulsificare, consta intr-o mica incizie in partea anterioara a ochiului, fragmentarea si aspiratia cristalinului cu ajutorul unei sonde cu ultrasunete. Implantul este apoi inserat (plasat) in locul cristalinului natural, in spatele pupilei. Incizia nu necesita de obicei sutura. Tehnica chirurgicala cel mai frecvent utilizata, numita facoemulsificare, consta intr-o mica incizie in partea anterioara a ochiului, fragmentarea si aspiratia cristalinului cu ajutorul unei sonde cu ultrasunete. Implantul este apoi inserat (plasat) in locul cristalinului natural, in spatele pupilei. Incizia nu necesita de obicei sutura.",
                1300,  d3);
        Service s14 = new Service("Schimb de cristalin", "In anumite situatii, cristalinul isi pierde proprietatile si astfel este afectata vederea, afectiunea vinovata fiind cataracta. Din fericire, acesta poate fi inlocuit cu unul artificial. Operatia de schimbare a cristalinului se efectueaza prin metoda numita facoemulsificare cu ultrasunete, dureaza aproximativ 7 minute, se face cu anestezie locala, fara internare, pacientul pleacand acasa dupa operatie. Este obligatoriu sa poarte pansament la ochiul operat pana ziua urmatoare cand se prezinta la controlul postoperator. Rata de reusita a acestei operatii este foarte mare, de peste 99%.",
                2200,  d3);

        serviceRepository.save(s1);
        serviceRepository.save(s2);
        serviceRepository.save(s3);
        serviceRepository.save(s4);
        serviceRepository.save(s5);
        serviceRepository.save(s6);
        serviceRepository.save(s7);
        serviceRepository.save(s8);
        serviceRepository.save(s9);
        serviceRepository.save(s10);
        serviceRepository.save(s11);
        serviceRepository.save(s12);
        serviceRepository.save(s13);
        serviceRepository.save(s14);



        Date h1 = new Date(118,0,1);  h1.setHours(13);
        Date h2 = new Date(118,1,12);  h2.setHours(12);
        Date h3 = new Date(118,2,15);  h3.setHours(11);
        Date h4 = new Date(118,3,23);  h4.setHours(16);
        Date h5 = new Date(118,6,23);  h5.setHours(16);
        Date h6 = new Date(118,7,1);  h6.setHours(13);
        Date h7 = new Date(118,7,1);  h7.setHours(15);
        Date h8 = new Date(118,7,1);  h8.setHours(10);
        Date h9 = new Date(118,7,1);  h9.setHours(11);
        Date h10 = new Date(118,7,1);  h10.setHours(18);

        Date h11 = new Date(118,1,1);  h11.setHours(13);
        Date h12 = new Date(118,8,8);  h12.setHours(16);

        Date h13 = new Date(118,1,1);  h13.setHours(12);
        Date h14 = new Date(118,4,12);  h14.setHours(10);
        Date h15 = new Date(118,10,14);  h15.setHours(17);

        Date h16 = new Date(118,8,13);  h16.setHours(9);

        Date h17 = new Date(118,11,1);  h17.setHours(13);
        Date h18 = new Date(118,2,13);  h18.setHours(19);
        Date h19 = new Date(118,1,23);  h19.setHours(12);
        Date h20 = new Date(118,6,13);  h20.setHours(16);

        Date h21 = new Date(118,1,1);  h21.setHours(16);
        Date h22 = new Date(118,8,2);  h22.setHours(17);

        Date h23 = new Date(118,3,18);  h23.setHours(13);
        Date h24= new Date(118,6,12);  h24.setHours(11);
        Date h25 = new Date(118,7,13);  h25.setHours(16);

        Date h26 = new Date(118,1,12);  h26.setHours(12);
        Date h27 = new Date(118,11,13);  h27.setHours(10);
        Date h28= new Date(118,10,11);  h28.setHours(11);
        Date h29 = new Date(118,8,18);  h29.setHours(18);
        Date h30 = new Date(118,7,1);  h30.setHours(14);

        Date h31 = new Date(118,5,1);  h31.setHours(13);
        Date h32= new Date(118,7,12);  h32.setHours(12);
        Date h33 = new Date(118,3,13);  h33.setHours(17);
        Date h34 = new Date(118,7,11);  h34.setHours(11);




        Consultation c1 = new Consultation(m1, p1, s4, h4);
        Consultation c2 = new Consultation(m1, p5, s5, h5);
        Consultation c3 = new Consultation(m1, p4, s1, h6);
        Consultation c4 = new Consultation(m1, p1, s2, h7);
        Consultation c5 = new Consultation(m1, p2, s3, h8);
        Consultation c6 = new Consultation(m1, p3, s1, h9);
        Consultation c7 = new Consultation(m1, p2, s1, h10);

        Consultation c8 = new Consultation(m2, p1, s4, h11);
        Consultation c9 = new Consultation(m2, p2, s5, h12);
        Consultation c10 = new Consultation(m2, p1, s1, h1);


        Consultation c11 = new Consultation(m3, p3, s1, h13);
        Consultation c12 = new Consultation(m3, p4, s2, h14);
        Consultation c13 = new Consultation(m3, p5, s1, h15);
        Consultation c14 = new Consultation(m3, p2, s2, h2);


        Consultation c15 = new Consultation(m4, p1, s3, h16);
        Consultation c16 = new Consultation(m4, p3, s3, h3);

        Consultation c17 = new Consultation(m5, p2, s11, h17);
        Consultation c18 = new Consultation(m5, p3, s9, h18);
        Consultation c19 = new Consultation(m5, p4, s10, h19);
        Consultation c20 = new Consultation(m5, p5, s11, h20);

        Consultation c21 = new Consultation(m6, p3, s11, h21);
        Consultation c22 = new Consultation(m6, p1, s9, h22);

        Consultation c23 = new Consultation(m7, p2, s9, h23);
        Consultation c24 = new Consultation(m7, p4, s11, h24);
        Consultation c25 = new Consultation(m7, p5, s9, h25);

        Consultation c26 = new Consultation(m8, p2, s12, h26);
        Consultation c27 = new Consultation(m8, p3, s13, h27);
        Consultation c28 = new Consultation(m8, p4, s12, h28);
        Consultation c29 = new Consultation(m8, p5, s14, h29);
        Consultation c30 = new Consultation(m8, p1, s12, h30);

        Consultation c31 = new Consultation(m9, p2, s13, h31);
        Consultation c32 = new Consultation(m9, p3, s12, h32);
        Consultation c33 = new Consultation(m9, p4, s13, h33);
        Consultation c34 = new Consultation(m9, p5, s14, h34);



        consultationRepository.save(c1);
        consultationRepository.save(c2);
        consultationRepository.save(c3);
        consultationRepository.save(c4);
        consultationRepository.save(c5);
        consultationRepository.save(c6);
        consultationRepository.save(c7);
        consultationRepository.save(c8);
        consultationRepository.save(c9);
        consultationRepository.save(c10);
        consultationRepository.save(c11);
        consultationRepository.save(c12);
        consultationRepository.save(c13);
        consultationRepository.save(c14);
        consultationRepository.save(c15);
        consultationRepository.save(c16);
        consultationRepository.save(c17);
        consultationRepository.save(c18);
        consultationRepository.save(c19);
        consultationRepository.save(c20);
        consultationRepository.save(c21);
        consultationRepository.save(c22);
        consultationRepository.save(c22);
        consultationRepository.save(c23);
        consultationRepository.save(c24);
        consultationRepository.save(c25);
        consultationRepository.save(c26);
        consultationRepository.save(c27);
        consultationRepository.save(c28);
        consultationRepository.save(c29);
        consultationRepository.save(c30);
        consultationRepository.save(c31);
        consultationRepository.save(c32);
        consultationRepository.save(c33);
        consultationRepository.save(c34);


        Diagnostic de0 = new Diagnostic("Diverse (a se specifica in simptome)", "Altele decat cele din lista", "any");
        Diagnostic de1 = new Diagnostic("Raceala", "Raceala este cea mai cunoscuta si mai comuna boala, ale carei semne sunt recunoscute cu usurinta de fiecare. Ea se manifesta prin: dureri de gat sau inrosirea acestuia, dureri de cap si febra, infundarea nasului sau scurgerea de secretii nazale, stranut, respiratie dificila pe nas si uneori febra.", "Medicina Generala");
        Diagnostic de2 = new Diagnostic("Gripa", "Gripă – infecție respiratorie virală contagioasă care atacă sistemul respirator: nasul, gâtul și plămânii. De obicei, afecțiunea se vindecă fără a fi nevoie de intervenții medicale. Uneori însă, poate conduce la complicații periculoase și chiar la deces.", "Medicina Generala");
        Diagnostic de3 = new Diagnostic("Bronho pneumonie", "Bronhopneumonia reprezinta o inflamatie acuta a plamanilor si a bronsiolelor, de obicei fiind rezultatul raspandirii infectiei localizate la nivelul tractului respirator superior catre cel inferior. Cu toate ca bronhopneumonia se aseamana cu pneumonia, poate fi mult mai periculoasa, necesitand supraveghere si tratament medical diferentiat.", "Medicina Generala");
        Diagnostic de4 = new Diagnostic("Viroza respiratorie", "Raceala este o infectie acuta virala usoara si autolimitata a tractului respirator superioar (nas, gat) care determina simptome variabile de tip stranut, nas infundat, secretii nazale, gat iritat, febra mica si dureri de cap. Raceala este o afectiune des intalnita in tara noastra in special in sezonul rece.", "Medicina Generala");
        Diagnostic de5 = new Diagnostic("Faringita acuta", "Faringita acuta este o infectie virala ce cuprinde ansamblul cailor aeriene superioare, de la fosele nazale la laringe, de aceea denumirea corecta ar fi cea de rinofaringita acuta.", "Medicina Generala");
        Diagnostic de6 = new Diagnostic("Cistita", "Cistita este o inflamație a mucoasei din interiorul vezicii urinare. Este confundată deseori cu anexita, inflamația localizată la nivelul organelor genitale, fiindcă principalul simptom, cel de disconfort abdominal, este similar.", "Medicina Generala");
        Diagnostic de7 = new Diagnostic("Lombago acut", "Lumbago acut este o durere lombara acuta, survenind dupa o miscare gresita si provocata de un microtraumatism care afecteaza un disc intervertebral. Aceasta durere cu aparitie brusca este provocata de o fisura a annulusului prin care se infiltreaza o parte din nucleus pulpos.", "Medicina Generala");
        Diagnostic de8 = new Diagnostic("Colica renala", "Colica reno-ureterala reprezinta o durere lombo-abdominala produsa de iritatia renala sau ureterala produsa de pietrele de la nivelul acestora care se manifesta sub forma de colica renala (se produce ca urmare a miscarii pietrelor (litiaza renala).", "Medicina Generala");
        Diagnostic de9 = new Diagnostic("dermatita post intepatura", "Dermatita poate fi tradusa simplu ca o inflamatie a pielii, insa aceasta afectiune este caracterizata printr-o multitudine de simptome precum: piele uscata, hiperemie (piele rosie), prurit (mancarime), cruste si alte leziuni exfoliative", "Medicina Generala");
        Diagnostic de10 = new Diagnostic("Otita", "Otita interna este mai degraba cunoscuta sub denumirea de labirintita. Aceasta este caracterizata de inflamatia urechii interne. In general, acest tip de otita dispare de la sine, fara sa fie nevoie de tratament.", "Medicina Generala");

        Diagnostic de11 = new Diagnostic("Conjunctivita", "Conjunctivita reprezinta inflamatia membranei conjunctive care captuseste globul ocular si pleoapele. Membrana conjunctiva este subtire, transparenta, cu rolul de a proteja si a pastra umiditatea ochiului.", "Oftalmologie");
        Diagnostic de12 = new Diagnostic("Conjunctivita acuta", "Conjunctivita reprezinta inflamatia membranei conjunctive care captuseste globul ocular si pleoapele. Membrana conjunctiva este subtire, transparenta, cu rolul de a proteja si a pastra umiditatea ochiului.", "Oftalmologie");


        diagnosticRepository.save(de0);
        diagnosticRepository.save(de1);
        diagnosticRepository.save(de2);
        diagnosticRepository.save(de3);
        diagnosticRepository.save(de4);
        diagnosticRepository.save(de5);
        diagnosticRepository.save(de6);
        diagnosticRepository.save(de7);
        diagnosticRepository.save(de8);
        diagnosticRepository.save(de9);
        diagnosticRepository.save(de10);
        diagnosticRepository.save(de11);
        diagnosticRepository.save(de12);


    }
}