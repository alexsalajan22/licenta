package com.alex.medkeep;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MedkeepApplication {

    public static void main(String[] args) {
        SpringApplication.run(MedkeepApplication.class, args);
    }
}
