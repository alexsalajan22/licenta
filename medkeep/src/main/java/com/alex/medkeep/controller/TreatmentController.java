package com.alex.medkeep.controller;

import com.alex.medkeep.model.Treatment;
import com.alex.medkeep.service.TreatmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TreatmentController {

    @Autowired
    private TreatmentService treatmentService;

    @RequestMapping(value = "/getalltreatments", method = RequestMethod.GET)
    public List<Treatment> findAllDiagnostics() {
        return treatmentService.getAllTreatments();
    }
}
