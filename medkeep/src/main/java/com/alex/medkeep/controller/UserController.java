package com.alex.medkeep.controller;

import com.alex.medkeep.model.Medic;
import com.alex.medkeep.model.Patient;
import com.alex.medkeep.model.Service;
import com.alex.medkeep.model.User;
import com.alex.medkeep.service.ServiceService;
import com.alex.medkeep.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/getallmedics", method = RequestMethod.GET)
    public List<User> findAllMedics() {
        return userService.getAllMedics();
    }

    @RequestMapping(value = "/getallpatients", method = RequestMethod.GET)
    public List<User> findAllPatients() {
        return userService.getAllPatients();
    }

    @RequestMapping(value = "/getallusers", method = RequestMethod.GET)
    public List<User> findAllUsers() {
        return userService.getAllUsers();
    }

    @RequestMapping(value = "/createpatient", method = RequestMethod.POST)
    public String addPatient(@RequestBody Patient patient){
        try{
            userService.addUser(patient);
            return "Successful";
        } catch (Exception e) {
            return "Failed:" + e.getMessage();
        }
    }

    @RequestMapping(value = "/findUserById", method=RequestMethod.POST)
    public User getUserbyId(@RequestBody Long id) {
        return this.userService.findUserById(id);
    }
}

