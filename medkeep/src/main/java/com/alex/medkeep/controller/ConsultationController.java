package com.alex.medkeep.controller;

import com.alex.medkeep.dto.DepartmentMedicConsultations;
import com.alex.medkeep.dto.MedicConsultations;
import com.alex.medkeep.model.Consultation;
import com.alex.medkeep.service.ConsultationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ConsultationController {

    @Autowired
    private ConsultationService consultationService;

    @RequestMapping(value = "/createconsultation", method = RequestMethod.POST)
    public String addConsultation(@RequestBody Consultation consultation){
        try{
            consultationService.addConsultation(consultation);
            return "Successful";
        } catch (Exception e) {
            return "Failed:" + e.getMessage();
        }
    }

    @RequestMapping(value = "/getallconsultations", method = RequestMethod.GET)
    public List<Consultation> findAllConsultations() {
        return consultationService.getAllConsultations();
    }

//    @RequestMapping(value = "/getmedicsconsultations", method = RequestMethod.GET)
//    public List<MedicConsultations> findMedicsConsultations() {
//        return consultationService.getMedicsWithConsultations();
//    }

    @RequestMapping(value = "/getdmc", method = RequestMethod.GET)
    public List<DepartmentMedicConsultations> findMedicsConsultations() {
        return consultationService.getMedicsWithConsultations();
    }
}
