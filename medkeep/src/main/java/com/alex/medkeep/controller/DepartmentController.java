package com.alex.medkeep.controller;

import com.alex.medkeep.model.Department;
import com.alex.medkeep.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    @RequestMapping(value = "/getalldepartments", method = RequestMethod.GET)
    public List<Department> findAllDepartments() {
        return departmentService.getAllDepartments();
    }
}
