package com.alex.medkeep.controller;

import com.alex.medkeep.model.Diagnostic;
import com.alex.medkeep.service.DiagnosticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DiagnosticController {

    @Autowired
    private DiagnosticService diagnosticService;

    @RequestMapping(value = "/getalldiagnostics", method = RequestMethod.GET)
    public List<Diagnostic> findAllDiagnostics() {
        return diagnosticService.getAllDiagnostics();
    }
}
