package com.alex.medkeep.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alex.medkeep.dto.Mail;
import com.alex.medkeep.utils.MailUtil;


@RestController
public class MailController {
    @RequestMapping(value = "/mailSender", method = RequestMethod.POST)
    public String sendMail(@RequestBody Mail mail) {
        try {
            MailUtil.sendMail(mail.getSubject(), mail.getBody(), mail.getReceiver());
            return "Successfully Sent E-mails";
        }catch (Exception e){
            return "Failed to Send E-mails";
        }
    }

}