package com.alex.medkeep.controller;

import com.alex.medkeep.model.Service;
import com.alex.medkeep.service.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;

@RestController
public class ServiceController {

    @Autowired
    private ServiceService serviceService;

    @RequestMapping(value = "/getallservices", method = RequestMethod.GET)
    public List<Service> findAllServices() {
        return serviceService.getAllServices();
    }
}
