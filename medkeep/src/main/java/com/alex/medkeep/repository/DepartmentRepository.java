package com.alex.medkeep.repository;

import com.alex.medkeep.model.Department;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepartmentRepository extends JpaRepository<Department, Long>{

    Department findByName (String name);
    Department findByIdDepartment(Long id);
}
