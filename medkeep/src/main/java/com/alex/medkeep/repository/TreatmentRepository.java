package com.alex.medkeep.repository;

import com.alex.medkeep.model.Treatment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TreatmentRepository extends JpaRepository<Treatment, Long> {

    List<Treatment> findAllByCategory(String category);
    Treatment findByName(String name);
    Treatment findByIdTreatment(Long id);
}
