package com.alex.medkeep.repository;

import com.alex.medkeep.model.Department;
import com.alex.medkeep.model.Medic;
import com.alex.medkeep.model.User;
import com.alex.medkeep.model.User.UserType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {

    List<User> findAllByUserType(UserType userType);

    List<User> findAllByFirstNameAndLastName(String firstName, String lastName);

    User findByEmail(String email);

    List<Medic> findAllMedicsByDepartment(Department department);

}
