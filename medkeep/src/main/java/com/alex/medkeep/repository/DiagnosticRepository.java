package com.alex.medkeep.repository;

import com.alex.medkeep.model.Diagnostic;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DiagnosticRepository extends JpaRepository<Diagnostic, Long> {

    List<Diagnostic> findAllByCategory(String category);
    Diagnostic findByName(String name);
    Diagnostic findByIdDiagnostic(Long id);
}
