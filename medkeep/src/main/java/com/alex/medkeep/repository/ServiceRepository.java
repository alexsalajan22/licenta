package com.alex.medkeep.repository;

import com.alex.medkeep.model.Department;
import com.alex.medkeep.model.Service;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ServiceRepository extends JpaRepository<Service, Long> {

    List<Service> findAllByDepartment(Department department);
    Service findByIdService(Long id);
    Service findByName(String name);

}
