package com.alex.medkeep.repository;

import com.alex.medkeep.dto.MedicConsultations;
import com.alex.medkeep.model.Consultation;
import com.alex.medkeep.model.Diagnostic;
import com.alex.medkeep.model.Medic;
import com.alex.medkeep.model.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ConsultationRepository extends JpaRepository<Consultation, Long> {

    List<Consultation> findAllByPatient(Patient patient);
    List<Consultation> findAllByMedic(Medic medic);

//    @Query("select c.clients from Consultation c where c.subscriptionId  in  (?1)")
//    public List<Client> findClients(List<Long> list);
//
//    @Query("SELECT new com.alex.medkeep.dto.MedicConsultations(c.fullName, c.nrOfConsultations) FROM Consultation c")
//    List<MedicConsultations> getMedicConsultations();

    @Query(value = "SELECT d.name, COUNT(c.idmedic), u.firstname, u.lastname FROM user u " +
            " INNER JOIN department d ON u.iddepartment = d.iddepartment" +
            " INNER JOIN consultation c ON u.iduser = c.idmedic" +
            " GROUP BY d.name, c.idmedic", nativeQuery = true)
    public List<Object[]> findMedicsConsultations();
}
