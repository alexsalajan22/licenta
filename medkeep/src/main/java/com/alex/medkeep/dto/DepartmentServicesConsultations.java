package com.alex.medkeep.dto;

import java.util.ArrayList;

public class DepartmentServicesConsultations {

    private String departmentName;
    private ArrayList<String> serviceNameList;
    private ArrayList<Integer> nrConsultationsList;

    public DepartmentServicesConsultations (String departmentName, ArrayList<String> serviceNames, ArrayList<Integer> nrOfConsultations) {
        this.departmentName = departmentName;
        this.serviceNameList = serviceNames;
        this.nrConsultationsList = nrOfConsultations;
    }
}
