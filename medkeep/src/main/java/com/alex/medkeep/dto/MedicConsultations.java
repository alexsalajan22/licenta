package com.alex.medkeep.dto;

public class MedicConsultations {

    private String fullName;
    private int nrOfConsultations;

    public MedicConsultations (String fullName, int nrOfConsultations) {
        this.fullName = fullName;
        this.nrOfConsultations = nrOfConsultations;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getNrOfConsultations() {
        return nrOfConsultations;
    }

    public void setNrOfConsultations(int nrOfConsultations) {
        this.nrOfConsultations = nrOfConsultations;
    }
}
