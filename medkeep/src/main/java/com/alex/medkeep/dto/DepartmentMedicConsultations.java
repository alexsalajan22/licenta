package com.alex.medkeep.dto;

import java.util.ArrayList;

public class DepartmentMedicConsultations {

    private String departmentName;
    private ArrayList<String> medicNameList;
    private ArrayList<Integer> nrConsultationsList;

    public DepartmentMedicConsultations(String departmentName, ArrayList<String> fullNameList, ArrayList<Integer> nrConsultationsList){
        this.departmentName = departmentName;
        this.medicNameList = fullNameList;
        this.nrConsultationsList = nrConsultationsList;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public ArrayList<String> getMedicNameList() {
        return medicNameList;
    }

    public void setMedicNameList(ArrayList<String> medicNameList) {
        this.medicNameList = medicNameList;
    }

    public ArrayList<Integer> getNrConsultationsList() {
        return nrConsultationsList;
    }

    public void setNrConsultationsList(ArrayList<Integer> nrConsultationsList) {
        this.nrConsultationsList = nrConsultationsList;
    }
}
