package com.alex.medkeep.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@DiscriminatorValue(value = "PATIENT")
public class Patient extends User {

    private static final long serialVersionUID = 1L;
    private String cnp;
    private String adress;
    private String city;
    private List<Consultation> consultationList = new ArrayList<>();

    public Patient(){
        super();
    }

    public Patient(String firstName, String lastName, Date birthDate, String email, String phoneNumber, String cnp, String adress, String city){
        super(firstName,lastName,birthDate,email,phoneNumber);
        this.cnp = cnp;
        this.adress = adress;
        this.city = city;
    }


    @Column(name = "cnp")
    public String getCnp() {
        return cnp;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }

    /**
     * @Transient ignores the missing setPassword() ; we do not need it here.
     * At LogIn the password will be the CNP of the patient
     */
    @Transient
    public String getPassword(){
        return this.cnp;
    }

    @Column(name = "adress")
    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    @Column(name = "city")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "patient", cascade = CascadeType.PERSIST)
    public List<Consultation> getConsultationList() {
        return consultationList;
    }

    public void setConsultationList(List<Consultation> consultationList) {
        this.consultationList = consultationList;
    }

}
