package com.alex.medkeep.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "department")
public class Department implements Serializable {

    private static final long serialVersionUID = 3371998938019434314L;
    private Long idDepartment;
    private String name;
    private String description;
    private List<Medic> medicList = new ArrayList<>();
    private List<Service> serviceList = new ArrayList<>();

    public Department (String name, String description){
        this.name = name;
        this.description = description;
    }

    public Department (){
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "iddepartment", unique = true, nullable = false)
    public Long getIdDepartment() {
        return idDepartment;
    }

    public void setIdDepartment(Long idDepartment) {
        this.idDepartment = idDepartment;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "department", cascade = CascadeType.PERSIST)
    public List<Medic> getMedicList() {
        return medicList;
    }

    public void setMedicList(List<Medic> medicList) {
        this.medicList = medicList;
    }

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "department", cascade = CascadeType.PERSIST)
    public List<Service> getServiceList() {
        return serviceList;
    }

    public void setServiceList(List<Service> serviceList) {
        this.serviceList = serviceList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return Objects.equals(idDepartment, that.idDepartment) &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description) &&
                Objects.equals(medicList, that.medicList);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idDepartment, name, description, medicList);
    }

    @Override
    public String toString() {
        return "Department{" +
                "idDepartment=" + idDepartment +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", medicList=" + medicList +
                '}';
    }
}
