package com.alex.medkeep.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;


@Entity
@Table(name = "treatment")
public class Treatment implements Serializable {

    private static final long serialVersionUID = 3371998938019434323L;
    private Long idTreatment;
    private String name;
    private String description;
    private double price;
    private String category;

    public  Treatment(){
    }

    public Treatment(String name, String description, double price, String category){
        this.name = name;
        this.description = description;
        this.price = price;
        this.category = category;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idtreatment", unique = true, nullable = false)
    public Long getidTreatment() {
        return idTreatment;
    }

    public void setidTreatment(Long idTreatment){
        this.idTreatment = idTreatment;
    }
//    public void setidTreatment(Long idTreatment) {
//        this.idTreatment = idTreatment;
//    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column (name = "price")
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Column(name = "category")
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Treatment treatment = (Treatment) o;
        return Double.compare(treatment.price, price) == 0 &&
                Objects.equals(idTreatment, treatment.idTreatment) &&
                Objects.equals(name, treatment.name) &&
                Objects.equals(description, treatment.description) &&
                Objects.equals(category, treatment.category);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idTreatment, name, description, price, category);
    }

    @Override
    public String toString() {
        return "Treatment{" +
                "idTreatment=" + idTreatment +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", category='" + category + '\'' +
                '}';
    }
}
