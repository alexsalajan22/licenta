package com.alex.medkeep.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Date;

@Entity
@DiscriminatorValue(value = "RECEPTIONIST")
public class Receptionist extends User{

    private static final long serialVersionUID = 1L;
    private String password;
    private String adress;

    public Receptionist(){
        super();
    }

    public Receptionist(String firstName, String lastName, Date birthDate, String email, String phoneNumber, String password, String adress){
        super(firstName,lastName,birthDate,email,phoneNumber);
        this.password = password;
        this.adress = adress;
    }


    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "adress")
    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }
}
