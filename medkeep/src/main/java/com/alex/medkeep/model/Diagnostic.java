package com.alex.medkeep.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "diagnostic")
public class Diagnostic implements Serializable{

    private static final long serialVersionUID = 3371998938019434323L;
    private Long idDiagnostic;
    private String name;
    private String description;
    private String category;

    public  Diagnostic(){
    }

    public Diagnostic(String name, String description, String category){
        this.name = name;
        this.description = description;
        this.category = category;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "iddiagnostic", unique = true, nullable = false)
    public Long getIdDiagnostic() {
        return idDiagnostic;
    }

    public void setIdDiagnostic(Long idDiagnostic){
        this.idDiagnostic = idDiagnostic;
    }
//    public void setIdDiagnostic(Long idDiagnostic) {
//        this.idDiagnostic = idDiagnostic;
//    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "category")
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Diagnostic that = (Diagnostic) o;
        return Objects.equals(idDiagnostic, that.idDiagnostic) &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description) &&
                Objects.equals(category, that.category);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idDiagnostic, name, description, category);
    }

    @Override
    public String toString() {
        return "Diagnostic{" +
                "idDiagnostic=" + idDiagnostic +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", category='" + category + '\'' +
                '}';
    }
}
