package com.alex.medkeep.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@DiscriminatorValue(value = "MEDIC")
public class Medic extends User {
    private String password;
    private String specialization;
    private String description;
    private Department department;


    public Medic(){
        super();
    }

    public Medic(String firstName, String lastName, Date birthDate, String email, String phoneNumber,
                 String password, String specialization, String description, Department department){
        super(firstName, lastName, birthDate, email, phoneNumber);
        this.password = password;
        this.specialization = specialization;
        this.description = description;
        this.department = department;
    }

    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "specialization")
    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "iddepartment")
    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }




}
