package com.alex.medkeep.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Date;

@Entity
@DiscriminatorValue(value = "ADMIN")
public class Administrator extends User{
    private String password;

    public Administrator(){
        super();
    }

    public Administrator(String firstName, String lastName, Date birthDate, String email, String phoneNumber, String password) {
        super(firstName, lastName, birthDate, email, phoneNumber);
        this.password = password;
    }

    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
