package com.alex.medkeep.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "consultation")
public class Consultation implements Serializable{
    private Long id;
    private Medic medic;
    private Patient patient;
    private Service service;
    private Date dateTime;
    private String symptoms;
    private String prescription;
    private Date sickLeaveStart;
    private Date sickLeaveEnd;
    private Diagnostic diagnostic;


    public Consultation(){
    }

    public Consultation(Medic medic, Patient patient, Service service, Date dateTime) {
        this.medic = medic;
        this.patient = patient;
        this.service = service;
        this.dateTime = dateTime;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "id", unique = true, nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne
    @JoinColumn(name="idmedic")
    public Medic getMedic() {
        return medic;
    }

    public void setMedic(Medic medic) {
        this.medic = medic;
    }

    @ManyToOne
    @JoinColumn(name="idpatient")
    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    @ManyToOne
    @JoinColumn(name="idservice")
    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Column (name = "datetime")
    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    @Column (name = "symptoms")
    public String getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(String symptoms) {
        this.symptoms = symptoms;
    }

    @Column (name = "prescription")
    public String getPrescription() {
        return prescription;
    }

    public void setPrescription(String prescription) {
        this.prescription = prescription;
    }

    @Column (name = "sickleavestart")
    public Date getSickLeaveStart() {
        return sickLeaveStart;
    }

    public void setSickLeaveStart(Date sickLeaveStart) {
        this.sickLeaveStart = sickLeaveStart;
    }

    @Column (name = "sickleaveend")
    public Date getSickLeaveEnd() {
        return sickLeaveEnd;
    }

    public void setSickLeaveEnd(Date sickLeaveEnd) {
        this.sickLeaveEnd = sickLeaveEnd;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "iddiagnostic")
    public Diagnostic getDiagnostic() {
        return diagnostic;
    }

    public void setDiagnostic(Diagnostic diagnostic) {
        this.diagnostic = diagnostic;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Consultation that = (Consultation) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(medic, that.medic) &&
                Objects.equals(patient, that.patient) &&
                Objects.equals(service, that.service) &&
                Objects.equals(dateTime, that.dateTime) &&
                Objects.equals(symptoms, that.symptoms) &&
                Objects.equals(prescription, that.prescription) &&
                Objects.equals(sickLeaveStart, that.sickLeaveStart) &&
                Objects.equals(sickLeaveEnd, that.sickLeaveEnd) &&
                Objects.equals(diagnostic, that.diagnostic);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, medic, patient, service, dateTime, symptoms, prescription, sickLeaveStart, sickLeaveEnd, diagnostic);
    }

    @Override
    public String toString() {
        return "Consultation{" +
                "id=" + id +
                ", medic=" + medic +
                ", patient=" + patient +
                ", service=" + service +
                ", dateTime=" + dateTime +
                ", symptoms='" + symptoms + '\'' +
                ", prescription='" + prescription + '\'' +
                ", sickLeaveStart=" + sickLeaveStart +
                ", sickLeaveEnd=" + sickLeaveEnd +
                ", diagnostic=" + diagnostic +
                '}';
    }
}
