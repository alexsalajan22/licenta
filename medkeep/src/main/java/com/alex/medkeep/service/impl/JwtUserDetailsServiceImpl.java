package com.alex.medkeep.service.impl;

import com.alex.medkeep.model.Administrator;
import com.alex.medkeep.model.User;
import com.alex.medkeep.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    	User user = null;
    	if(username.equals("admin")){
    		user = new Administrator();
    		user.setEmail("admin");
    		PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    		String hashedPassword = passwordEncoder.encode("admin");
    		user.setUserType(User.UserType.ADMIN);
    		((Administrator) user).setPassword(hashedPassword);
    	}
    	else
    	{
    		user = userRepository.findByEmail(username);
    		
    	}
        if (user == null) {
            throw new UsernameNotFoundException(String.format("No user found with username '%s'.", username));
        } else {
            return user;
        }
    }
}
