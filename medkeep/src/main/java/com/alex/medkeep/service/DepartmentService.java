package com.alex.medkeep.service;

import com.alex.medkeep.model.Department;

import java.util.List;

public interface DepartmentService {

    List<Department> getAllDepartments();

    Department findDepartmentById(Long id);
    Department findDepartmentByName(String name);

    void addDepartment(Department department);
    void deleteDepartment(Department department);

}
