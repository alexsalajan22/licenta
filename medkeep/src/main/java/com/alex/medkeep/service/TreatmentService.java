package com.alex.medkeep.service;

import com.alex.medkeep.model.Treatment;

import java.util.List;

public interface TreatmentService {

    List<Treatment> getAllTreatments();
    List<Treatment> getAllTreatmentsByCategory(String category);

    Treatment findTreatmentById(Long id);
    Treatment findTreatmentByName(String name);

    void addTreatment(Treatment treatment);
    void deleteTreatment(Treatment treatment);
}
