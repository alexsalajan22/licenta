package com.alex.medkeep.service;

import com.alex.medkeep.model.Department;
import com.alex.medkeep.model.Service;

import java.util.List;

public interface ServiceService {

    List<Service> getAllServices();
    List<Service> getAllServicesByDepartment(Department department);

    Service findServiceById(Long id);
    Service findServiceByName(String name);

    void addService(Service service);
    void deleteService(Service service);
}
