package com.alex.medkeep.service.impl;

import com.alex.medkeep.model.Treatment;
import com.alex.medkeep.repository.TreatmentRepository;
import com.alex.medkeep.service.TreatmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TreatmentServiceImpl implements TreatmentService {

    @Autowired
    private TreatmentRepository treatmentRepository;

    @Override
    public List<Treatment> getAllTreatments() {
        return treatmentRepository.findAll();
    }

    @Override
    public List<Treatment> getAllTreatmentsByCategory(String category) {
        return treatmentRepository.findAllByCategory(category);
    }

    @Override
    public Treatment findTreatmentById(Long id) {
        return treatmentRepository.findByIdTreatment(id);
    }

    @Override
    public Treatment findTreatmentByName(String name) {
        return treatmentRepository.findByName(name);
    }

    @Override
    public void addTreatment(Treatment treatment) {
        this.treatmentRepository.save(treatment);
    }

    @Override
    public void deleteTreatment(Treatment diagnostic) {
        this.treatmentRepository.delete(diagnostic);
    }
}
