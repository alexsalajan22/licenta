package com.alex.medkeep.service;

import com.alex.medkeep.model.Diagnostic;

import java.util.List;
import java.util.Locale;

public interface DiagnosticService {
    List<Diagnostic> getAllDiagnostics();
    List<Diagnostic> getAllDiagnosticsByCategory(String category);

    Diagnostic findDiagnosticById(Long id);
    Diagnostic findDiagnosticByName(String name);

    void addDiagnostic(Diagnostic diagnostic);
    void deleteDiagnostic(Diagnostic diagnostic);

}
