package com.alex.medkeep.service;

import com.alex.medkeep.dto.DepartmentMedicConsultations;
import com.alex.medkeep.dto.MedicConsultations;
import com.alex.medkeep.model.Consultation;
import com.alex.medkeep.model.Medic;
import com.alex.medkeep.model.Patient;

import java.util.List;

public interface ConsultationService {

    List<Consultation> getAllConsultations();
    List<Consultation> getAllConsultationsByPatient(Patient patient);
    List<Consultation> getAllConsultationsByMedic(Medic medic);

    void addConsultation(Consultation consultation);
    void deleteConsultation(Consultation consultation);

    List<DepartmentMedicConsultations> getMedicsWithConsultations();
}
