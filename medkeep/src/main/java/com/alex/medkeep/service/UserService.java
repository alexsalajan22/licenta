package com.alex.medkeep.service;

import com.alex.medkeep.model.Department;
import com.alex.medkeep.model.Medic;
import com.alex.medkeep.model.User;

import java.util.List;

public interface UserService {

    List<User> getAllPatients();
    List<User> getAllMedics();
    List<User> getAllUsers();
    List<User> getAllReceptionists();
    List<Medic> getAllUsersByDepartment(Department department);
    List<User> findUsersByFullName(String firstName, String lastName);

    User addUser(User user);
    void deleteUser(User user);

    User findUserByEmail(String email);
    User findUserById(Long id);

}
