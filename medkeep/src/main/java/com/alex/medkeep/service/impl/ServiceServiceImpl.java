package com.alex.medkeep.service.impl;

import com.alex.medkeep.model.Department;
import com.alex.medkeep.model.Service;
import com.alex.medkeep.repository.ServiceRepository;
import com.alex.medkeep.service.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@org.springframework.stereotype.Service
public class ServiceServiceImpl implements ServiceService {
    @Autowired
    private ServiceRepository serviceRepository;

    @Override
    public List<Service> getAllServices() {
        return serviceRepository.findAll();
    }

    @Override
    public List<Service> getAllServicesByDepartment(Department department) {
        return serviceRepository.findAllByDepartment(department);
    }

    @Override
    public Service findServiceById(Long id) {
        return serviceRepository.findByIdService(id);
    }

    @Override
    public Service findServiceByName(String name) {
        return serviceRepository.findByName(name);
    }

    @Override
    public void addService(Service service) {
        serviceRepository.save(service);
    }

    @Override
    public void deleteService(Service service) {
        serviceRepository.delete(service);
    }
}
