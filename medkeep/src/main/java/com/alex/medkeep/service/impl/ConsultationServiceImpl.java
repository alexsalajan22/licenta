package com.alex.medkeep.service.impl;

import com.alex.medkeep.dto.DepartmentMedicConsultations;
import com.alex.medkeep.dto.MedicConsultations;
import com.alex.medkeep.model.Consultation;
import com.alex.medkeep.model.Medic;
import com.alex.medkeep.model.Patient;
import com.alex.medkeep.repository.ConsultationRepository;
import com.alex.medkeep.service.ConsultationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ConsultationServiceImpl implements ConsultationService {
    @Autowired
    private ConsultationRepository consultationRepository;

    @Override
    public List<Consultation> getAllConsultations() {
        return consultationRepository.findAll();
    }

    @Override
    public List<Consultation> getAllConsultationsByPatient(Patient patient) {
        return consultationRepository.findAllByPatient(patient);
    }

    @Override
    public List<Consultation> getAllConsultationsByMedic(Medic medic) {
        return consultationRepository.findAllByMedic(medic);
    }

    @Override
    public void addConsultation(Consultation consultation) {
        consultationRepository.save(consultation);
    }

    @Override
    public void deleteConsultation(Consultation consultation) {
        consultationRepository.delete(consultation);
    }

    @Override
    public List<DepartmentMedicConsultations> getMedicsWithConsultations() {
        List<DepartmentMedicConsultations> medicsConsultations = new ArrayList<>();
        List<Object[]> data = consultationRepository.findMedicsConsultations();
        Map<String, ArrayList> medics = new HashMap<>();
        Map<String, ArrayList> consultations = new HashMap<>();
        for (Object[] columns : data) {
            String department = (String) columns[0];
            BigInteger count2 = (BigInteger)columns[1];
            int count = count2.intValue();
            String firstName = (String) columns[2];
            String lastName = (String) columns[3];
            if (medics.get(department) == null) {
                medics.put(department, new ArrayList<>());
                consultations.put(department, new ArrayList<>());
            }
            medics.get(department).add(firstName + " " + lastName);
            consultations.get(department).add(count);
        }
        for (String department: medics.keySet()) {
            DepartmentMedicConsultations cons = new DepartmentMedicConsultations(department,
                    medics.get(department), consultations.get(department));
            medicsConsultations.add(cons);
        }
        return medicsConsultations;
    }
}
