package com.alex.medkeep.service.impl;

import com.alex.medkeep.model.*;
import com.alex.medkeep.repository.UserRepository;
import com.alex.medkeep.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;


    @Override
    public List<User> getAllPatients() {
        return userRepository.findAllByUserType(User.UserType.PATIENT);
    }

    @Override
    public List<User> getAllMedics() {
        return userRepository.findAllByUserType(User.UserType.MEDIC);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public List<User> getAllReceptionists() {
        return userRepository.findAllByUserType(User.UserType.RECEPTIONIST);
    }

    @Override
    public List<Medic> getAllUsersByDepartment(Department department) {
        return userRepository.findAllMedicsByDepartment(department);
    }

    @Override
    public List<User> findUsersByFullName(String firstName, String lastName) {
        return userRepository.findAllByFirstNameAndLastName(firstName,lastName);
    }

    @Override
    public User addUser(User user) {
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        switch (user.getUserType()) {
            case ADMIN:
                Administrator admin = (Administrator) user;
                admin.setPassword(passwordEncoder.encode(admin.getPassword()));
                return userRepository.save(admin);
            case MEDIC:
                Medic medic = (Medic) user;
                medic.setPassword(passwordEncoder.encode(medic.getPassword()));
                return userRepository.save(medic);
            case PATIENT:
                Patient patient = (Patient) user;
                return userRepository.save(patient);
            case RECEPTIONIST:
                Receptionist receptionist = (Receptionist) user;
                receptionist.setPassword(passwordEncoder.encode(receptionist.getPassword()));
                return userRepository.save(receptionist);
            default:
                throw new AssertionError("The User type entered is invalid");
        }
    }

    @Override
    public void deleteUser(User user) {
        userRepository.delete(user);
    }

    @Override
    public User findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public User findUserById(Long id) {
        return userRepository.findOne(id);
    }
}
