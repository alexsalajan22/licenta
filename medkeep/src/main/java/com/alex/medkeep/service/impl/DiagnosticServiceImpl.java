package com.alex.medkeep.service.impl;

import com.alex.medkeep.model.Diagnostic;
import com.alex.medkeep.repository.DiagnosticRepository;
import com.alex.medkeep.service.DiagnosticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DiagnosticServiceImpl implements DiagnosticService{

    @Autowired
    private DiagnosticRepository diagnosticRepository;

    @Override
    public List<Diagnostic> getAllDiagnostics() {
        return diagnosticRepository.findAll();
    }

    @Override
    public List<Diagnostic> getAllDiagnosticsByCategory(String category) {
        return diagnosticRepository.findAllByCategory(category);
    }

    @Override
    public Diagnostic findDiagnosticById(Long id) {
        return diagnosticRepository.findByIdDiagnostic(id);
    }

    @Override
    public Diagnostic findDiagnosticByName(String name) {
        return diagnosticRepository.findByName(name);
    }

    @Override
    public void addDiagnostic(Diagnostic diagnostic) {
        diagnosticRepository.save(diagnostic);
    }

    @Override
    public void deleteDiagnostic(Diagnostic diagnostic) {
        diagnosticRepository.delete(diagnostic);
    }
}
