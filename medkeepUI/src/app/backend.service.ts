import { Headers, Http, RequestOptions } from '@angular/http';
import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
// import { catchError, map, tap } from 'rxjs/operators';
// import 'rxjs/add/observable/throw';
// import 'rxjs/add/operator/map';
import { map } from 'rxjs/operators';



import { Service } from './/models/service';
import { Medic, User, Patient } from './models/user';
import { Department } from './models/department';
import { Consultation } from './models/consultation';
import { Diagnostic } from './models/diagnostic';
import { MedicConsultation } from './models/medicConsultations';
import { Treatment } from './models/treatment';
import { DepartmentMedicConsultations } from './models/DepartmentMedicConsultations';
import { Mail } from './models/mail';



@Injectable({
    providedIn: 'root'
})
export class BackendService {
    link = 'http://localhost:9123/';

    private headers = new Headers({
        'Content-Type': 'application/json',
        // 'Authorization': 'Bearer ' + this.authenticationService.getToken()
    });
    private options = new RequestOptions({ withCredentials: true, headers: this.headers });

    constructor(private http: Http,
        private router: Router) { }

    public getAllServices(): Observable<[Service]> {
        return this.http.get(this.link + 'getallservices', this.options).pipe(map(response => response.json()));
        // console.log('sda', this.http);
    }

    public getAllConsultations(): Observable<[Consultation]> {
        return this.http.get(this.link + 'getallconsultations', this.options).pipe(map(response => response.json()));
        // console.log('sda', this.http);
    }

    public getAllMedics(): Observable<[Medic]> {
        return this.http.get(this.link + 'getallmedics', this.options).pipe(map(response => response.json()));
        // console.log('sda', this.http);
    }

    public getAllPatients(): Observable<[Patient]> {
        return this.http.get(this.link + 'getallpatients', this.options).pipe(map(response => response.json()));
        // console.log('sda', this.http);
    }


    public getAllUsers(): Observable<[User]> {
        return this.http.get(this.link + 'getallusers', this.options).pipe(map(response => response.json()));
        // console.log('sda', this.http);
    }

    public getAllDiagnostics(): Observable<[Diagnostic]> {
        return this.http.get(this.link + 'getalldiagnostics', this.options).pipe(map(response => response.json()));
        // console.log('sda', this.http);
    }

    public getAllDepartments(): Observable<[Department]> {
        return this.http.get(this.link + 'getalldepartments', this.options).pipe(map(response => response.json()));
        // console.log('get Departments', this.http);
    }

    public getAllTreatments(): Observable<[Treatment]> {
        return this.http.get(this.link + 'getalltreatments', this.options).pipe(map(response => response.json()));
        // console.log('get Departments', this.http);
    }

     /**
     * returneaza o lista de DTO-uri care reprezinta numele medicului si numarul de consultatii ale lui.
     * stearsa din backend consultationController.
     */
    public getMedicsAndConsultations(): Observable<[MedicConsultation]> {
        return this.http.get(this.link + 'getmedicsconsultations', this.options).pipe(map(response => response.json()));
        // console.log('sda', this.http);
    }

     /**
     * returneaza o lista de DTO-uri care reprezinta numele medicului si numarul de consultatii ale lui.
     */
    public getDepartmentMedicConsultations(): Observable<[DepartmentMedicConsultations]> {
        return this.http.get(this.link + 'getdmc', this.options).pipe(map(response => response.json()));
        // console.log('sda', this.http);
    }

    public addUser(uri: string, user: User): Observable<any> {
        return this.http.post(this.link + uri, user, this.options).pipe(map(response => response.text()));
    }

    public addConsultation( consultation: Consultation): Observable<any> {
        return this.http.post(this.link + 'createconsultation', consultation, this.options).pipe(map(response => response.text()));
    }

    public updateConsultation( consultation: Consultation): Observable<any> {
        return this.http.post(this.link + 'createconsultation', consultation, this.options).pipe(map(response => response.text()));
    }

    public getUserById(id: number) {
        return this.http.post(this.link + 'findUserById', id, this.options).pipe(map(response => response.json()));
    }

    public sendMail(mailUtil: Mail) {
        return this.http.post(this.link + 'mailSender', mailUtil, this.options).pipe(map(response => response.text()));
    }








    // private handleError(error: any): Observable<any> {
    //     if (error.type === 3) {
    //         // nu avem drepturi sa intram pe pagina asta, sau ne-a expirat tokenul din BE
    //         if (this.authenticationService.isLoggedIn()) {
    //             // ne-a expirat tokenul
    //             this.router.navigate(['/login'], { queryParams: { returnUrl: this.router.routerState.snapshot.url, error: 'true' } });
    //         } else {
    //             // nu avem acces, ne intoarcem pe homepage
    //             this.router.navigate(['/home']);
    //         }

    //     }
    //     return error.message || error;
    // }
}
