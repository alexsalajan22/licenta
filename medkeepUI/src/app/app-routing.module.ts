import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { ServiceComponent } from './pages/service/service.component';
import { HomeComponent } from './pages/home/home.component';
import { MedicsComponent } from './pages/medics/listmedics/medics.component';
import { ContactComponent } from './pages/contact/contact.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { CreateUserComponent } from './pages/user/create-user/create-user.component';
import { NewconsultationComponent } from './pages/consultation/newconsultation/newconsultation.component';
import { ListconsultationsComponent } from './pages/consultation/listconsultations/listconsultations.component';
import { UserconsultationsComponent } from './pages/user/userconsultations/userconsultations.component';
import { GeneralstatisticsComponent } from './pages/statistics/generalstatistics/generalstatistics.component';
import { StatisticsComponent } from './pages/statistics/statistics/statistics.component';

import { CarouselComponent } from './pages/carousel/carousel.component';
import { AlltreatmentsComponent } from './pages/treatment/alltreatments/alltreatments.component';


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full'},
  { path: 'services', component: ServiceComponent},
  { path: 'home', component: HomeComponent},
  { path: 'medics', component: MedicsComponent},
  { path: 'contact', component: ContactComponent},
  { path: 'login', component: LoginPageComponent},
  { path: 'createuser', component: CreateUserComponent},
  { path: 'newconsultation', component: NewconsultationComponent},
  { path: 'carousel' , component: CarouselComponent},
  { path: 'consultations' , component: ListconsultationsComponent},
  { path: 'userconsultations' , component: UserconsultationsComponent},
  { path: 'generalstatistics' , component: GeneralstatisticsComponent},
  { path: 'treatments' , component: AlltreatmentsComponent},
  { path: 'statistics' , component: StatisticsComponent},

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    CommonModule
  ],
  exports: [ RouterModule ],
  declarations: []
})
export class AppRoutingModule { }
