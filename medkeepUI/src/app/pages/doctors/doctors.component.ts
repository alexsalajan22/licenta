import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

import { Medic } from '../../models/user';
import { BackendService } from '../../backend.service';

@Component({
  selector: 'app-medics',
  templateUrl: './medics.component.html',
  styleUrls: ['./medics.component.css']
})
export class MedicsComponent implements OnInit {

  allMedics: Medic[] = [];

  constructor(private backendService: BackendService) { }

  ngOnInit() {
    this.refreshMedics();
  }

  refreshMedics(): void {
    // showPleaseWait();
    this.backendService.getAllMedics().subscribe(res => {
      this.allMedics = res;
      console.log(this.allMedics);
      // hidePleaseWait();
    });
  }

}


