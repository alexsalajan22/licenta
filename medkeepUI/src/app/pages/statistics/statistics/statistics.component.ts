import { Component, OnInit } from '@angular/core';
import { MedicConsultation } from '../../../models/medicConsultations';
import { BackendService } from '../../../backend.service';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit {

  data: any;
  options: any;
  md: MedicConsultation = new MedicConsultation('Florn', 5);
  medicsConsultations: MedicConsultation[] = [];
  allLabels: string[] = [];
  allData: number[] = [];

  constructor(private backendService: BackendService) {

  }

  ngOnInit() {
    this.getMedicsConsultations();
    setTimeout(() => {



      const ctx = document.getElementById('myChart');
      const myChart = new Chart(ctx, {
          type: 'bar',
          data : {
            labels: this.allLabels,
            datasets: [
              {
                label: 'Number of Consultations',
                backgroundColor: '#42A5F5',
                borderColor: '#1E88E5',
                data: this.allData
              }
            ]
          },
          options: {
            responsive: true,
            maintainAspectRatio: false,
            title: {
                display: true,
                text: 'Statistics About Our Medics',
                fontSize: 20
            },
            legend: {
                position: 'top'
            },
            scales: {
              yAxes: [{
                  display: true,
                  ticks: {
                      // suggestedMin: 0,    // minimum will be 0, unless there is a lower value.
                      // OR //
                      beginAtZero: true   // minimum value will be 0.
                  }
              }]
          }
        }
      });

    }, 100);
  }



  //     this.data = {
  //       labels: this.allLabels,
  //       datasets: [
  //         {
  //           label: 'Number of Consultations',
  //           backgroundColor: '#42A5F5',
  //           borderColor: '#1E88E5',
  //           data: this.allData
  //         }
  //       ]
  //     };
  //     this.options = {
  //       responsive: true,
  //       // maintainAspectRatio: false,
  //       title: {
  //           display: true,
  //           text: 'Statistics About Our Medics',
  //           fontSize: 20
  //       },
  //       legend: {
  //           position: 'top'
  //       },
  //       scales: {
  //         yAxes: [{
  //             display: true,
  //             ticks: {
  //                 // suggestedMin: 0,    // minimum will be 0, unless there is a lower value.
  //                 // OR //
  //                 beginAtZero: true   // minimum value will be 0.
  //             }
  //         }]
  //     }
  //   };
  //   }, 100);
  // }

  getMedicsConsultations() {
    this.medicsConsultations = [];
    this.backendService.getMedicsAndConsultations().subscribe(res => {
      res.forEach(element => {
        this.medicsConsultations.push(element);
      });
    });
    console.log(this.medicsConsultations);
    setTimeout(() => {
      this.populateData();
      this.populateLabels();
    }, 100);
  }


  populateLabels() {
    this.allLabels = [];
    this.medicsConsultations.forEach(element => {
      this.allLabels.push(element.fullName);
    });
    console.log(this.allLabels);
  }

  populateData() {
    this.allData = [];
    this.medicsConsultations.forEach(element => {
      this.allData.push(element.nrOfConsultations);
    });
    console.log(this.allData);
  }


}
