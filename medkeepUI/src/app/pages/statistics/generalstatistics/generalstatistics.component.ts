import { Component, OnInit } from '@angular/core';
import { MedicConsultation } from '../../../models/medicConsultations';
import { BackendService } from '../../../backend.service';
import { Chart } from 'chart.js';
import { DepartmentMedicConsultations } from '../../../models/DepartmentMedicConsultations';

@Component({
  selector: 'app-generalstatistics',
  templateUrl: './generalstatistics.component.html',
  styleUrls: ['./generalstatistics.component.css'],

})
export class GeneralstatisticsComponent implements OnInit {

  data: any;
  options: any;
  md: MedicConsultation = new MedicConsultation('Florn', 5);
  medicsConsultations: MedicConsultation[] = [];
  allLabels: string[] = [];
  allData: number[] = [];

  departmentMedicConsultaions: DepartmentMedicConsultations[] = [];
  departments: string[] = [];
  medics: string[] = [];
  nrConsultations: number[][] = [[]];
  nrConsultations2 =  [];
  // departments: string[][] = [[]];
  // medics: string[][][] = [[[]]];
  // nrConsultations: number[][][] = [[[]]];

  constructor(private backendService: BackendService) {

  }

  ngOnInit() {
    this.getDepartmentMedicConsultations();
    // this.getMedicsConsultations();
    // this.populate();
    setTimeout(() => {

      const ctx = document.getElementById('myChart');
      const myChart = new Chart(ctx, {
          type: 'bar',
          data : {
            labels: this.departments,
            datasets: [
              {
                label: this.medics[0],
                backgroundColor: '#42A5F5',
                borderColor: '#1E88E5',
                data: this.nrConsultations[0]
              },
              {
                label: this.medics[1],
                backgroundColor: '#96B5F5',
                borderColor: '#1E88E5',
                data: this.nrConsultations[1]
              },
              {
                label: this.medics[2],
                backgroundColor: '#4215F5',
                borderColor: '#1E88E5',
                data: this.nrConsultations[2]
              },
              {
                label: this.medics[3],
                backgroundColor: '#EB35F5',
                borderColor: '#1E88E5',
                data: this.nrConsultations[3]
              },
              {
                label: this.medics[4],
                backgroundColor: '#099f98',
                borderColor: '#1E88E5',
                data: this.nrConsultations[4]
              },
              {
                label: this.medics[5],
                backgroundColor: '#cf6b2e',
                borderColor: '#1E88E5',
                data: this.nrConsultations[5]
              },
              {
                label: this.medics[6],
                backgroundColor: '#4f7e9a',
                borderColor: '#1E88E5',
                data: this.nrConsultations[6]
              },
              {
                label: this.medics[7],
                backgroundColor: '#6eef2a',
                borderColor: '#1E88E5',
                data: this.nrConsultations[7]
              },
              {
                label: this.medics[8],
                backgroundColor: '#ecfb31',
                borderColor: '#1E88E5',
                data: this.nrConsultations[8]
              },

              // {
              //   label: this.medics[1],
              //   backgroundColor: '#42A5F5',
              //   borderColor: '#1E88E5',
              //   data: this.nrConsultations
              // }
            ],
        },
          options: {
            responsive: true,
            maintainAspectRatio: false,
            title: {
                display: true,
                text: 'Nr. of consultations / medic, sorted by departments',
                fontSize: 20
            },
            legend: {
                position: 'top'
            },
            scales: {
              yAxes: [{
                  display: true,
                  ticks: {
                      // suggestedMin: 0,    // minimum will be 0, unless there is a lower value.
                      // OR //
                      beginAtZero: true   // minimum value will be 0.
                  }
              }]
          }
        }
      });

    }, 500);



    setTimeout(() => {

      const ctx = document.getElementById('myChart2');
      const myChart2 = new Chart(ctx, {
          type: 'bar',
          data : {
            labels: this.medics,
            datasets: [
              {
                label: 'Number of Consultations / medic',
                backgroundColor: '#42A5F5',
                borderColor: '#1E88E5',
                data: this.nrConsultations2
              }


              // {
              //   label: this.medics[1],
              //   backgroundColor: '#42A5F5',
              //   borderColor: '#1E88E5',
              //   data: this.nrConsultations
              // }
            ],
        },
          options: {
            responsive: true,
            maintainAspectRatio: false,
            title: {
                display: true,
                text: 'Nr. of consultations / medic',
                fontSize: 20
            },
            legend: {
                position: 'top'
            },
            scales: {
              yAxes: [{
                  display: true,
                  ticks: {
                      // suggestedMin: 0,    // minimum will be 0, unless there is a lower value.
                      // OR //
                      beginAtZero: true   // minimum value will be 0.
                  }
              }]
          }
        }
      });

    }, 500);

  }



  //     this.data = {
  //       labels: this.allLabels,
  //       datasets: [
  //         {
  //           label: 'Number of Consultations',
  //           backgroundColor: '#42A5F5',
  //           borderColor: '#1E88E5',
  //           data: this.allData
  //         }
  //       ]
  //     };
  //     this.options = {
  //       responsive: true,
  //       // maintainAspectRatio: false,
  //       title: {
  //           display: true,
  //           text: 'Statistics About Our Medics',
  //           fontSize: 20
  //       },
  //       legend: {
  //           position: 'top'
  //       },
  //       scales: {
  //         yAxes: [{
  //             display: true,
  //             ticks: {
  //                 // suggestedMin: 0,    // minimum will be 0, unless there is a lower value.
  //                 // OR //
  //                 beginAtZero: true   // minimum value will be 0.
  //             }
  //         }]
  //     }
  //   };
  //   }, 100);
  // }

  populate() {
    this.departments = [];
    this.medics = [];
    this.nrConsultations = [];
    this.departments = ['dep1', 'dep2', 'dep3'];
    this.medics = ['Salajan', 'Marian', 'Marcel', 'Pavel', 'Banica', 'cristian', 'Mihai', 'Moraru', 'Miruna'];
    this.nrConsultations = [[2, 0, 0], [4, 0, 0], [6, 0, 0], [0, 5, 0], [0, 12, 0], [0, 3, 0], [0, 0, 9], [0, 0, 14], [0, 0, 7]];
    // this.nrConsultations = [[2, 0, 0], [0, 4, 0], [0, 0, 6], [0, 5, 0], [0, 12, 0], [0, 3, 0], [0, 0, 9], [0, 0, 14], [0, 0, 7]];
    }

  getDepartmentMedicConsultations() {
    this.departmentMedicConsultaions = [];
    this.backendService.getDepartmentMedicConsultations().subscribe( res => {
      res.forEach(element => {
        const nm = new DepartmentMedicConsultations(element.departmentName, element.medicNameList, element.nrConsultationsList);
        this.departmentMedicConsultaions.push(nm);
      });
      console.log(this.departmentMedicConsultaions);
    });
    console.log(this.departmentMedicConsultaions);
    setTimeout(() => {
      this.populateData();
      this.populateChart2();
    }, 200);
  }

  populateData() {
    this.medics = [];
    this.nrConsultations = [];
    this.departments = [];
    this.departmentMedicConsultaions.forEach(element => {
      this.departments.push(element.departmentName);
    });
    this.departmentMedicConsultaions.forEach(element => {
      const medics = element.medicNameList;
      medics.forEach(medic => {
        this.medics.push(medic);
        // this.medics.push('Mihai Matei"', 'Cristian Chirita', 'Marchis Ionescu', 'Oltean Mic', 'Troba Anastasia', 'Popescu Ioan');
      });
      const numbers = element.nrConsultationsList;
      // let finalnumbers = [[]];
      if (element.departmentName == this.departmentMedicConsultaions[0].departmentName) {
        numbers.forEach(nr => {
          this.nrConsultations.push([nr, 0, 0]);
          // this.nrConsultations.push( [4, 0, 0], [6, 0, 0]);
        });
      }
      if (element.departmentName == this.departmentMedicConsultaions[1].departmentName) {
        numbers.forEach(nr => {
          this.nrConsultations.push([0, nr, 0]);
          // this.nrConsultations.push( [0, 12, 0], [0, 3, 0]);
        });
      }
      if (element.departmentName == this.departmentMedicConsultaions[2].departmentName) {
        numbers.forEach(nr => {
          this.nrConsultations.push([0, 0, nr]);
          // this.nrConsultations.push( [0, 0, 14], [0, 0, 7]);
        });
      }
      // this.nrConsultations.push([4, 0, 0], [6, 0, 0], [0, 5, 0], [0, 3, 0], [0, 0, 9], [0, 0, 14]);
    });
  }

  populateChart2() {
    this.departmentMedicConsultaions.forEach(element => {
      element.nrConsultationsList.forEach(element2 => {
        this.nrConsultations2.push(element2);
      });
    });
  }
  }





  // getMedicsConsultations() {
  //   this.medicsConsultations = [];
  //   this.backendService.getMedicsAndConsultations().subscribe(res => {
  //     res.forEach(element => {
  //       this.medicsConsultations.push(element);
  //     });
  //   });
  //   console.log(this.medicsConsultations);
  //   setTimeout(() => {
  //     this.populateData();
  //     this.populateLabels();
  //   }, 100);
  // }


  // populateLabels() {
  //   this.allLabels = [];
  //   this.medicsConsultations.forEach(element => {
  //     this.allLabels.push(element.fullName);
  //   });
  //   console.log(this.allLabels);
  // }

  // populateData() {
  //   this.allData = [];
  //   this.medicsConsultations.forEach(element => {
  //     this.allData.push(element.nrOfConsultations);
  //   });
  //   console.log(this.allData);
