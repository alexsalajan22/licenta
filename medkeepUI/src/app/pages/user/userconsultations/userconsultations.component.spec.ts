import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserconsultationsComponent } from './userconsultations.component';

describe('UserconsultationsComponent', () => {
  let component: UserconsultationsComponent;
  let fixture: ComponentFixture<UserconsultationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserconsultationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserconsultationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
