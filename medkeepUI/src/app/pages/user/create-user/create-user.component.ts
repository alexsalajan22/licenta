import { Component, OnInit } from '@angular/core';
import { BackendService } from '../../../backend.service';
import { User, Patient } from '../../../models/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {

  message = '';
  // createdUser: User = new User(0, '', '', null, '', '', 'PATIENT');
  createdUser: Patient = new Patient(0, '', '', null, '', '', '', '', '');
  patients: Patient[] = [];

  constructor(private router: Router, private backendService: BackendService) { }

  ngOnInit() {
    this.getAllPatients();
  }

  addUser() {
    if (!this.userExists(this.createdUser.email, this.createdUser.cnp)) {
    if (this.createdUser.firstName === '' || this.createdUser.lastName === '' || this.createdUser.email === '' ||
    this.createdUser.birthDate == null || this.createdUser.phoneNumber === '' || this.createdUser.city === '' ||
     this.createdUser.adress === '' || this.createdUser.email === '') {
       this.message = 'All fields are required';
       setTimeout(() => {
        this.message = '';
        console.log('All fields required');
      }, 2500);
      return false;
    } else {
    const uri = 'create' + this.createdUser.userType.toLowerCase(); // uri = createpatient
    this.backendService.addUser(uri, this.createdUser).subscribe(res => {
        this.message = res;
        setTimeout(() => {
          this.message = '';
          console.log('user added');
          this.router.navigate(['/login']);
        }, 2000);
      });
    }
  } else {
    this.message = 'User already registered with this email';
    setTimeout(() => {
      this.message = '';
    }, 3000);
  }
}

userExists (email: string, cnp: string) {
    let exists = false;
    this.patients.forEach(element => {
      if (element.email === email || element.cnp === cnp) {
        exists = true;
      }
    });
    return exists;
}

getAllPatients() {
  this.backendService.getAllPatients().subscribe(res => {
    this.patients = res;
  });
}
}
