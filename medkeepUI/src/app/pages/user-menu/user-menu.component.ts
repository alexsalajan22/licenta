import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../services/authentication.service';

class MenuCluster {
  label: string;
  children: MenuButton[];
  active = false;
}

class MenuButton {
  label: string;
  link: string;
}

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.css']
})
export class UserMenuComponent implements OnInit {

  buttons: MenuButton[] = [
    {
      label: 'Home',
      link: 'home'
    },
    {
      label: 'Services',
      link: 'services'
    },
    {
      label: 'Treatments',
      link: 'treatments'
    },
    {
      label: 'Doctors',
      link: 'medics'
    },
    {
      label: 'Contact',
      link: 'contact'
    }
  ];

  constructor(private router: Router, private authenticationService: AuthenticationService) {
  }

  ngOnInit() {
  }

  navigate(url: MenuButton) {
    this.router.navigate(['/' + url.link]);
  }

  toggleHover(cluster: MenuCluster, state: boolean): void {
    cluster.active = state;
  }

  notLoggedIn(): boolean {
    return !this.authenticationService.isLoggedIn();
  }

  isMedic(): boolean {
    return this.authenticationService.hasRole(['MEDIC']);
  }

  isPatient(): boolean {
    return this.authenticationService.hasRole(['PATIENT']);
  }

  isAdmin(): boolean {
    return this.authenticationService.hasRole(['ADMIN']);
  }
}
