import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication.service';
import { Consultation } from '../../../models/consultation';
import { BackendService } from '../../../backend.service';
import { Medic, Patient } from '../../../models/user';
import { Diagnostic } from '../../../models/diagnostic';
import { Department } from '../../../models/department';
import { Service } from '../../../models/service';

@Component({
  selector: 'app-listconsultations',
  templateUrl: './listconsultations.component.html',
  styleUrls: ['./listconsultations.component.css']
})
export class ListconsultationsComponent implements OnInit {

  allConsultations: Consultation[] = [];
  medicsConsultations: Consultation[] = [];
  medic: Medic = new Medic(0, '', '', null, '', '', '', '', '', null);
  date = '';
  time = '';
  selectedConsultation: Consultation = new Consultation(new Patient(0, '', '', new Date(), '', '', '', '', ''),
  new Medic(0, '', '', new Date(), '', '', '', '', '', new Department(0, '', '')),
  new Service(0, '', '', 0, new Department(0, '', '')), new Date());
  allDiagnostics: Diagnostic[] = [];
  message = '';
  selectedDiagnosticId: number = null;
  selectedDiagnostic: Diagnostic = new Diagnostic(0, '', '', '');

  constructor(private router: Router, private authenticationService: AuthenticationService, private backendservice: BackendService) {
  }

  ngOnInit() {
    this.selectedConsultation = null;
    this.setMedic();
    this.getConsultations();
  }

  navigate(url: string) {
    this.router.navigate(['/' + url]);
  }

  isMedic(): boolean {
    return this.authenticationService.hasRole(['MEDIC']);
  }

  setMedic(): void {
    const id: number = this.authenticationService.getId();
    this.backendservice.getAllMedics().subscribe(res => {
      res.forEach(element => {
        if (element.idUser === id) {
          this.medic = element;
        }
      });
    });
  }


  getConsultations() {
    this.selectedConsultation = null;
    this.allConsultations = [];
    this.medicsConsultations = [];
    this.backendservice.getAllConsultations().subscribe(rez => {
      this.allConsultations = rez;
      rez.forEach(element => {
        element.dateTime = new Date(element.dateTime);
        if (element.sickLeaveEnd != null && element.sickLeaveStart != null) {
          element.sickLeaveStart = new Date(element.sickLeaveStart);
          element.sickLeaveEnd = new Date(element.sickLeaveEnd);
        }
        element.patient.birthDate = new Date(element.patient.birthDate);
        if (element.medic.idUser === this.medic.idUser) {
          this.medicsConsultations.push(element);
        }
      });
      console.log(this.allConsultations);
    });
  }

  setDate(date: Date): String {
    const newDate = date.getDate() + '-' + date.getMonth() + '-' + date.getFullYear();
    return newDate;
  }

  setTime(date: Date): void {
    const newTime = date.getHours() + ' : ' + date.getMinutes();
    this.time = newTime;
  }

  fillConsultation(id: number) {
    this.selectedDiagnosticId = 0;
    this.getDiagnostics();
    this.medicsConsultations.forEach(element => {
      if (element.id === id && element != null) {
        this.selectedConsultation = element;
        if (this.selectedConsultation.diagnostic != null) {
        this.selectedDiagnosticId = this.selectedConsultation.diagnostic.idDiagnostic;
        }
      }
    });
  }

  getDiagnostics() {
    this.allDiagnostics = [];
    this.backendservice.getAllDiagnostics().subscribe(rez => {
      rez.forEach(element => {
        this.allDiagnostics.push(element);
      });
    });
  }

  submitConsultation() {
    this.setDiagnostic(this.selectedDiagnosticId);
    this.backendservice.updateConsultation(this.selectedConsultation).subscribe(res => {
      this.message = res;
      setTimeout(() => {
        if (res === 'Successful') {
          this.message = '';
          console.log('consultation added');
       }}, 2000);
    });
  }

  setDiagnostic(id: any) {
    if (typeof id == 'number' || this.selectedDiagnosticId > 0) {
     this.allDiagnostics.forEach(element => {
       if (this.selectedDiagnosticId == element.idDiagnostic) {
        //  let newDiagnostic: Diagnostic = new Diagnostic(element.idDiagnostic, element.name, element.description, element.category);
         this.selectedConsultation.diagnostic = element;
       }
     });
    } else {
      this.selectedDiagnosticId = 0;
      this.selectedConsultation.diagnostic = null;
    }
  }

  getNextConsultations() {
    this.selectedConsultation = null;
    this.medicsConsultations = [];
    const now = new Date().getTime();
    this.allConsultations.forEach(element => {
      element.dateTime = new Date(element.dateTime);
      if (element.medic.idUser === this.medic.idUser && element.dateTime.getTime() > now) {
        this.medicsConsultations.push(element);
      }
    });
    console.log(this.allConsultations);
  }

  getPastConsultations() {
    this.selectedConsultation = null;
    this.medicsConsultations = [];
    const now = new Date().getTime();
    this.allConsultations.forEach(element => {
      element.dateTime = new Date(element.dateTime);
      if (element.medic.idUser === this.medic.idUser && element.dateTime.getTime() < now) {
        this.medicsConsultations.push(element);
      }
    });
    console.log(this.allConsultations);
  }

}
