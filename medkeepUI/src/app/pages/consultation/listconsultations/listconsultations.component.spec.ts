import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListconsultationsComponent } from './listconsultations.component';

describe('ListconsultationsComponent', () => {
  let component: ListconsultationsComponent;
  let fixture: ComponentFixture<ListconsultationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListconsultationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListconsultationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
