import { Component, OnInit } from '@angular/core';
import { BackendService } from '../../../backend.service';
import { AuthenticationService } from '../../../services/authentication.service';
import { Router } from '@angular/router';

import { User } from '../../../models/user';
import { Patient } from '../../../models/user';
import { Medic } from '../../../models/user';
import { Service } from '../../../models/service';
import { Department } from '../../../models/department';
import { Consultation } from '../../../models/consultation';
import { Mail } from '../../../models/mail';


@Component({
  selector: 'app-newconsultation',
  templateUrl: './newconsultation.component.html',
  styleUrls: ['./newconsultation.component.css']
})
export class NewconsultationComponent implements OnInit {

  mail: Mail = new Mail('', '', '');
  allMedics: Medic[] = [];
  allMedicsByDepartment: Medic[] = [];
  allServices: Service[] = [];
  allServicesByDepartment: Service[] = [];
  allDepartments: Department[] = [];
  newServices: Service[] = [];
  newMedics: Medic[] = [];

  allConsultations: Consultation[] = [];
  allHours = [];
  selectedHour: 0;

  selectedDepartment: Department = new Department(0, 'any', '');

  newConsultation: Consultation = new Consultation(new Patient(0, '', '', new Date(), '', '', '', '', ''),
    new Medic(0, '', '', new Date(), '', '', '', '', '', new Department(0, '', '')),
    new Service(0, '', '', 0, new Department(0, '', '')), new Date());
  message = '';

  constructor(private router: Router, private backendService: BackendService, private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.getMedics();
    this.getDepartments();
    this.getServices();
    this.isLoggedIn();
    this.setLoggedPatient();
    this.getConsultations();
    // this.sendMail2();
  }

  getMedics(): void {
    this.backendService.getAllMedics().subscribe(res => {
      this.allMedics = res;
      this.allMedicsByDepartment = res;
      console.log(this.allMedics);
    });
  }

  getServices(): void {
    this.backendService.getAllServices().subscribe(res => {
      this.allServicesByDepartment = res;
      this.allServices = res;
      console.log(this.allServicesByDepartment);
    });
  }


  getDepartments(): void {
    this.backendService.getAllDepartments().subscribe(res => {
      this.allDepartments = res;
      console.log(this.allDepartments);
    });
  }

  getConsultations(): void {
    this.backendService.getAllConsultations().subscribe(res => {
      this.allConsultations = res;
      console.log(this.allConsultations);
    });
  }

  uploadDepartment($event) {
    // this.newConsultation = new Consultation(new Patient(0, '', '', new Date(), '', '', '', '', ''),
    // new Medic(0, '', '', new Date(), '', '', '', '', '', new Department(0, '', '')),
    // new Service(0, '', '', 0, new Department(0, '', '')), new Date());
    // Upload the Services by selected department:
    this.newServices = [];
    if (this.selectedDepartment.idDepartment > 0) {
      this.allServices.forEach(element => {
        if (element.department != null) {
          // tslint:disable-next-line:triple-equals
          if (element.department.idDepartment == this.selectedDepartment.idDepartment) {
            this.newServices.push(element);
          }
        }
      });
      if (this.newServices.length > 0) {
        this.allServicesByDepartment = this.newServices;
      } else {
        this.allServicesByDepartment = [];
      }
    } else {
      this.allServicesByDepartment = this.allServices;
    }

    // Upload the Medics by selected department:
    this.newMedics = [];
    if (this.selectedDepartment.idDepartment > 0) {
      this.allMedics.forEach(element => {
        if (element.department != null) {
          // tslint:disable-next-line:triple-equals
          if (element.department.idDepartment == this.selectedDepartment.idDepartment) {
            this.newMedics.push(element);
          }
        }
      });
      if (this.newMedics.length > 0) {
        this.allMedicsByDepartment = this.newMedics;
      } else {
        this.allMedicsByDepartment = [];
      }
    } else {
      this.allMedicsByDepartment = this.allMedics;
    }

  }

  isLoggedIn(): void {
    if (!this.authenticationService.hasRole(['PATIENT'])) {
      this.router.navigate(['/login']);
    }
  }


    // if (this.createdUser.firstName === '' || this.createdUser.lastName === '' || this.createdUser.email === '' ||
    // this.createdUser.birthDate == null || this.createdUser.phoneNumber === '' || this.createdUser.city === '' ||
    //  this.createdUser.adress === '' || this.createdUser.email === '') {
    //    this.message = 'All fields are required';
    //    setTimeout(() => {
    //     this.message = '';
    //     console.log('All fields required');
    //   }, 2500);
    //   return false;
    // } else {
    // }
    // newConsultation: Consultation = new Consultation(new Patient(0, '', '', new Date(), '', '', '', '', ''),
    // new Medic(0, '', '', new Date(), '', '', '', '', '', new Department(0, '', '')),
    // new Service(0, '', '', 0, new Department(0, '', '')), new Date());

  addConsultation() {
    this.setSelectedHour();
    this.newConsultation.service = this.getService(this.newConsultation.service.idService);
    this.newConsultation.medic = this.getMedic(this.newConsultation.medic.idUser);

    if ( this.newConsultation.service == null || this.newConsultation.medic == null || this.selectedDepartment.idDepartment === 0 ||
      this.selectedHour == null || this.newConsultation.service.idService === 0 ||
      this.newConsultation.medic.idUser === 0 || this.newConsultation.medic.firstName === '' || this.newConsultation.service.name === '' ) {
       this.message = 'All fields are required';
       this.newConsultation = new Consultation(new Patient(0, '', '', new Date(), '', '', '', '', ''),
       new Medic(0, '', '', new Date(), '', '', '', '', '', new Department(0, '', '')),
       new Service(0, '', '', 0, new Department(0, '', '')), new Date());
       this.setLoggedPatient();
          setTimeout(() => {
           this.message = '';
         }, 2500);
        //  this.ngOnInit();
         return false;
       } else {
    this.backendService.addConsultation(this.newConsultation).subscribe(res => {
      this.message = res;
      if (res === 'Successful') {
        this.sendMail();
        this.message = 'Successful. A confirmation email has been sent';
      }
      setTimeout(() => {
        if (res === 'Successful' || res === 'Successful. A confirmation email has been sent') {
          this.message = '';
          console.log('consultation added');
          this.router.navigate(['/home']);
       }}, 4000);
    });
  }
}

  getService(id: number): Service {
    let service: Service = null;
    this.allServices.forEach(element => {
      // tslint:disable-next-line:triple-equals
      if (element.idService == id) {
        service = element;
      }
    });
    return service;
  }

  getMedic(id: number): Medic {
    let medic: Medic = null;
    this.allMedics.forEach(element => {
      // tslint:disable-next-line:triple-equals
      if (element.idUser == id) {
        medic = element;
      }
    });
    return medic;
  }

  setLoggedPatient() {
    if (this.authenticationService.isLoggedIn()) {
      this.backendService.getUserById(this.authenticationService.getId()).subscribe(res => {
        this.newConsultation.patient = res;
        // this.getAllClientsTimetables();
      });
    }
  }

  getHoursByDateAndMedic($event) {
    this.selectedHour = null;
    this.allHours = [];
    let unAvailableHours = [];
   
    let selectedDateString: Date = this.newConsultation.dateTime;
    let selectedDate = new Date(selectedDateString);
    let selectedMedic: Medic = this.getMedic(this.newConsultation.medic.idUser);
    let consultationsByDateAndMedic: Consultation[] = [];
    this.allConsultations.forEach( element => {
      // tslint:disable-next-line:triple-equals
      let realDate: Date = new Date(element.dateTime);
      if (selectedMedic.idUser == element.medic.idUser && selectedDate.getMonth() == realDate.getMonth() &&
            selectedDate.getDay() == realDate.getDay()) {
          consultationsByDateAndMedic.push(element);
      }
    });

    consultationsByDateAndMedic.forEach(element => {
      let realDate: Date = new Date(element.dateTime);
      // tslint:disable-next-line:triple-equals
      unAvailableHours.push(realDate.getHours());
    });

    for (let i = 8 ; i <= 19; i++) {
      if (unAvailableHours.indexOf(i) == -1) {
        let json = {'hour': i, 'available': ''};
        this.allHours.push(json);
      } else {
        let json = {'hour': i, 'available': 'disabled'};
        this.allHours.push(json);
      }
    }
  }

  setSelectedHour() {
    let realDate: Date = new Date(this.newConsultation.dateTime);
    realDate.setHours(this.selectedHour);
    this.newConsultation.dateTime = realDate;
  }

  sendMail() {
    let receiver: string;
    let message: string;
    receiver = this.newConsultation.patient.email;
    this.mail.subject = 'programare noua la medkeep';
    this.mail.receiver = receiver;
    message = 'Buna ziua, \naveti o noua consultatie la data de ' + this.newConsultation.dateTime.getDate() + '/' +
              this.newConsultation.dateTime.getMonth() + '/' + this.newConsultation.dateTime.getFullYear() +
              ' ora: ' + this.selectedHour + ':00' + '\nServiciul consultatiei: ' + this.newConsultation.service.name +
              ' \nMedicul consultatiei: ' + this.newConsultation.medic.firstName + ' ' + this.newConsultation.medic.lastName +
              '\n \n O zis frumoasa, \n echipa Medkeep \n 0745623812 ';
    this.mail.body = message;

    this.backendService.sendMail(this.mail).subscribe(res => {
        console.log(res);
    // this.message = res;
    } );
  }

  sendMail2() {
    this.mail.body = 'sadfasdfasf';
    this.mail.receiver = 'alex_sala96@yahoo.com';
    this.mail.subject = 'subiect';
    this.backendService.sendMail(this.mail).subscribe(res => {
      console.log(res);
  this.message = res;
  } );
  }


}
