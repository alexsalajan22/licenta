import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';
import { GalleriaModule } from 'primeng/galleria';
import {CarouselModule} from 'primeng/carousel';
import { BackendService } from '../../backend.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  items: Array<any> = [];
  images: any[];
  imgs: string[] = [];
  nrMedics = 0;
  nrDepartments = 0;
  nrTreatments = 0;
  nrServices = 0 ;
  nrConsultations = 0;
  nrPatients = 0;



  constructor(private router: Router, private authenticationService: AuthenticationService, private backendService: BackendService) {

  }


  ngOnInit() {
    this.imgs = [];
    this.imgs.push('assets/images/gallery/login.jpg');
    this.imgs.push('assets/images/gallery/consultation.jpg');
    this.imgs.push('assets/images/gallery/consultation2.jpg');
    this.imgs.push('assets/images/gallery/consultation3.jpg');
    this.imgs.push('assets/images/gallery/img1.jpg');
    this.imgs.push('assets/images/gallery/img2.jpg');

    this.images = [];
    this.images.push({source: 'assets/images/galleria/treatments.jpg',
    alt: ' Modern treatments for a lot of diseases', title: 'Latest treatments' });
    this.images.push({source: 'assets/images/galleria/services.jpg',
    alt: ' More than 20 actual services', title: ' Comprehensive services'});
    this.images.push({source: 'assets/images/galleria/medics.jpg',
    alt: ' International award-winning specialists', title: 'A professional team'});
    this.images.push({source: 'assets/images/galleria/consultations.jpg',
    alt: ' Easy, fast and useful online appointments ', title: 'Online appointemnts'});
    this.images.push({source: 'assets/images/galleria/contact.jpg',
    alt: ' Contact us for any questions or concerns ', title: 'We are here for you!'});


    this.getMedics();
    this.getServics();
    this.getTreatments();
    this.getDepartments();
    this.getConsultations();
    this.getPatients();

    this.animateValue(200);

  }


  isAdminOrMedic(): boolean {
    return this.authenticationService.hasRole(['ADMIN', 'MEDIC']);
  }

  isPatient(): boolean {
    return this.authenticationService.hasRole(['PATIENT']);
  }

  isMedic(): boolean {
    return this.authenticationService.hasRole(['MEDIC']);
  }

  isAdmin(): boolean {
    return this.authenticationService.hasRole(['ADMIN']);
  }


  navigateTo(link: string): void {
    this.router.navigate([link]);
  }

  animateValue(end) {
    const duration = 2000;
    const start = 0;
    const range = end - start;
    let current = start;
    const increment = end > start ? 1 : -1;
    const stepTime = Math.abs(Math.floor(duration / range));
    // let obj = document.getElementById(id);
    const timer = setInterval(function() {
        current += increment;
        this.nrMedics = current;
        if (current === end) {
            clearInterval(timer);
        }
    }, stepTime);
}

  getMedics() {
    this.nrMedics = 0;
    this.backendService.getAllMedics().subscribe(res => {
      res.forEach(element => {
        this.nrMedics ++;
      });
    });
  }

  getServics() {
    this.nrServices = 0;
    this.backendService.getAllServices().subscribe(res => {
      res.forEach(element => {
        this.nrServices ++ ;
      });
    });
  }

  getTreatments() {
    this.nrTreatments = 0;
    this.backendService.getAllTreatments().subscribe(res => {
      res.forEach(element => {
        this.nrTreatments ++ ;
      });
    });
  }

  getDepartments() {
    this.nrDepartments = 0;
    this.backendService.getAllDepartments().subscribe(res => {
      res.forEach(element => {
        this.nrDepartments ++ ;
      });
    });
  }

  getConsultations() {
    this.nrConsultations = 0;
    this.backendService.getAllConsultations().subscribe(res => {
      res.forEach(element => {
        this.nrConsultations ++ ;
      });
    });
  }

  getPatients() {
    this.nrPatients = 0;
    this.backendService.getAllPatients().subscribe(res => {
      res.forEach(element => {
        this.nrPatients ++ ;
      });
    });
  }
}
