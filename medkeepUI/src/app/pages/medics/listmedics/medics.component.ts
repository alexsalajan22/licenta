import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

import { Medic } from '../../../models/user';
import { BackendService } from '../../../backend.service';
import { Department } from '../../../models/department';
import { element } from 'protractor';

@Component({
  selector: 'app-medics',
  templateUrl: './medics.component.html',
  styleUrls: ['./medics.component.css']
})
export class MedicsComponent implements OnInit {

  allMedics: Medic[] = [];
  allDepartments: Department[] = [];

  constructor(private backendService: BackendService) { }

  ngOnInit() {
    this.refreshMedics();
    this.getAllDepartments();
  }

  refreshMedics(): void {
    // showPleaseWait();
    this.backendService.getAllMedics().subscribe(res => {
      this.allMedics = res;
      console.log(this.allMedics);
      // hidePleaseWait();
    });
  }

  getAllDepartments() {
    this.allDepartments = [];
    this.backendService.getAllDepartments().subscribe( res => {
      res.forEach(dep => {
        this.allDepartments.push(dep);
      });
    });
  }

  getMedics(id: number) {
    this.allMedics = [];
    this.backendService.getAllMedics().subscribe(rez => {
      rez.forEach(medic => {
        if (medic.department.idDepartment == id) {
          this.allMedics.push(medic);
          document.getElementById('title').innerText = medic.department.name + ' Medics';
        }
      });
    });
  }

}
