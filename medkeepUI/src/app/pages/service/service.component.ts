import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

import { Service } from '../../models/service';
import { BackendService } from '../../backend.service';
import { Department } from '../../models/department';

declare var showPleaseWait: any;
declare var hidePleaseWait: any;


@Component({
  selector: 'app-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.css']
})
export class ServiceComponent implements OnInit {

  allServices: Service[] = [];
  allDepartments: Department[] = [];
  allShortServices: Service[] = [];
  selectedService: Service;

  constructor(private router: Router, private backendService: BackendService) { }

  ngOnInit() {
    this.refreshServices();
    this.getAllDepartments();
    this.getAllShortServices();
  }

  refreshServices(): void {
    // showPleaseWait();
    this.backendService.getAllServices().subscribe(res => {
      this.allServices = res;
      console.log(this.allServices);
      // hidePleaseWait();
    });
  }

  getAllDepartments() {
    this.allDepartments = [];
    this.backendService.getAllDepartments().subscribe( res => {
      res.forEach(dep => {
        this.allDepartments.push(dep);
      });
    });
  }

  getAllShortServices() {
    this.allShortServices = [];
    this.backendService.getAllServices().subscribe(res => {
      res.forEach(element => {
        element.description = element.description.substring(0, 230) + ' ... (click for more info)';
        this.allShortServices.push(element);
      });
    });
  }

  getServicesByDepartment(id: number) {
    this.allServices = [];
    this.allShortServices = [];
    this.backendService.getAllServices().subscribe(rez => {
      rez.forEach(service => {
        if (service.department.idDepartment === id) {
          this.allServices.push(service);
          document.getElementById('title').innerText = service.department.name + ' Services';
        }
      });

      });
      this.backendService.getAllServices().subscribe(rez => {
        rez.forEach(service => {
          if (service.department.idDepartment === id) {
            service.description = service.description.substring(0, 230) + ' ... (click for more info)';
            this.allShortServices.push(service);
          }
        });
        });

  }

  fillService(id: number) {
    this.selectedService = null;
    this.allServices.forEach(element => {
      if (element.idService === id) {
        this.selectedService = element;
      }
    });
    document.getElementById('buttonid').innerText = 'Call us for more info';
  }

  ChangeButtonName() {
    const x = document.getElementById('buttonid').innerText;
    if (x === 'Call us for more info') {
        document.getElementById('buttonid').innerText = '0746276312';
    } else {
        document.getElementById('buttonid').innerText = 'Call us for more info';
    }
}

goToAppointment() {
  this.router.navigate(['/newconsultation']);
}
}
