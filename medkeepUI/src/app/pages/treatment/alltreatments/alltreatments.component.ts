import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

import { BackendService } from '../../../backend.service';
import { Treatment } from '../../../models/treatment';


@Component({
  selector: 'app-alltreatments',
  templateUrl: './alltreatments.component.html',
  styleUrls: ['./alltreatments.component.css']
})
export class AlltreatmentsComponent implements OnInit {

  allTreatments: Treatment[] = [];
  allShortTreatments: Treatment[] = [];
  selectedTreatment: Treatment;

  constructor(private backendService: BackendService) { }

  ngOnInit() {
    this.getAllTreatments();
    this.getAllShortTreatments();
  }

  getAllTreatments() {
    this.allTreatments = [];
    this.backendService.getAllTreatments().subscribe(res => {
      res.forEach(element => {
        this.allTreatments.push(element);
      });
    });
  }

  getAllShortTreatments() {
    this.allShortTreatments = [];
    this.backendService.getAllTreatments().subscribe(res => {
      res.forEach(element => {
        element.description = element.description.substring(0, 230) + ' ... (click for more info)';
        this.allShortTreatments.push(element);
      });
    });
  }

  fillTreatment(id: number) {
    this.allTreatments.forEach(element => {
      if (element.idTreatment === id) {
        this.selectedTreatment = element;
        }
    });
    document.getElementById('buttonid').innerText = 'Contact us for more info';
  }

  ChangeButtonName() {
      const x = document.getElementById('buttonid').innerText;
      if (x === 'Contact us for more info') {
          document.getElementById('buttonid').innerText = '0746276312';
      } else {
          document.getElementById('buttonid').innerText = 'Contact us for more info';
      }
  }

}
