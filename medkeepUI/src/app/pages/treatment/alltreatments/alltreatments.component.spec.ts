import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlltreatmentsComponent } from './alltreatments.component';

describe('AlltreatmentsComponent', () => {
  let component: AlltreatmentsComponent;
  let fixture: ComponentFixture<AlltreatmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlltreatmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlltreatmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
