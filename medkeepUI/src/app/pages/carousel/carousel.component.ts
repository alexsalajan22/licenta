import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css']
})
export class CarouselComponent {

  items: Array<any> = [];

  constructor() {
    this.items = [
      { name: 'assets/images/consultation.jpg' },
      { name: 'assets/images/contactimage.png' },
      { name: 'assets/images/homepage.jpg' },
      { name: 'assets/images/login-page.jpg' }
    ];
  }
}
