import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { User } from '../../models/user';
import { BackendService } from '../../backend.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class HeaderComponent implements OnInit {

    loggedInUser: string;
    loggedIn = false;
    user = '';

    constructor(private authenticationService: AuthenticationService, private backendservice: BackendService) { }

    ngOnInit() {
        this.loggedInUser = this.authenticationService.getCurrentUser();
        this.setUser();
    }

    isLoggedIn(): boolean {
        return this.authenticationService.isLoggedIn();
    }

    getCurrentUser(): string {
        this.loggedInUser = this.authenticationService.getCurrentUser();
        return this.loggedInUser;
    }

    isMedic(): boolean {
        return this.authenticationService.hasRole(['MEDIC']);
    }

    isAdmin(): boolean {
        return this.authenticationService.hasRole(['ADMIN']);
    }

    setUser(): void {
        this.user = '';
        const id: number = this.authenticationService.getId();
        this.backendservice.getAllUsers().subscribe(res => {
            res.forEach(element => {
                if (element.idUser === id) {
                    this.user = element.firstName;
                }
            });
        });
    }

}
