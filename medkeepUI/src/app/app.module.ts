import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { ServiceComponent } from './pages/service/service.component';
import { AppRoutingModule } from './app-routing.module';
import { BackendService } from './backend.service';
import { MedicsComponent } from './pages/medics/listmedics/medics.component';
import { ContactComponent } from './pages/contact/contact.component';

import { AgmCoreModule } from '@agm/core';
import { UserMenuComponent } from './pages/user-menu/user-menu.component';
import { HeaderComponent } from './common/header/header.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { CreateUserComponent } from './pages/user/create-user/create-user.component';

import { AuthenticationService } from './services/authentication.service';
import { NewconsultationComponent } from './pages/consultation/newconsultation/newconsultation.component';
// import { AuthGuard } from './services/auth.guard';
// import {SessionStorageService} from 'ngx-webstorage';

import { Ng2CarouselamosModule } from 'ng2-carouselamos';
import { CarouselComponent } from './pages/carousel/carousel.component';
import { ListconsultationsComponent } from './pages/consultation/listconsultations/listconsultations.component';
import { UserconsultationsComponent } from './pages/user/userconsultations/userconsultations.component';
import { GeneralstatisticsComponent } from './pages/statistics/generalstatistics/generalstatistics.component';

import {ChartModule} from 'primeng/chart';
import { AlltreatmentsComponent } from './pages/treatment/alltreatments/alltreatments.component';
import { StatisticsComponent } from './pages/statistics/statistics/statistics.component';
import { GalleriaModule } from 'primeng/galleria';
import {CarouselModule} from 'primeng/carousel';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ServiceComponent,
    MedicsComponent,
    ContactComponent,
    UserMenuComponent,
    HeaderComponent,
    LoginPageComponent,
    CreateUserComponent,
    NewconsultationComponent,
    CarouselComponent,
    ListconsultationsComponent,
    UserconsultationsComponent,
    GeneralstatisticsComponent,
    AlltreatmentsComponent,
    StatisticsComponent,

  ],
  imports: [
    Ng2CarouselamosModule,
    HttpModule,
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ChartModule,
    GalleriaModule,
    CarouselModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyApwlCTzdpCR8h56dLwU4ON_eIWzxoNW5c'
    })
  ],
  providers: [
    BackendService,
    AuthenticationService,
    // AuthGuard;
    // SessionStorageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
