export class Diagnostic {
    idDiagnostic: number;
    name: string;
    description: string;
    category: string;

    constructor(id: number, name: string, description: string, category: string) {
        this.idDiagnostic = id;
        this.name = name;
        this.description = description;
        this.category = category;
    }
}
