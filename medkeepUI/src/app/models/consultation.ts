import { Medic, Patient } from './user';
import { Service } from './service';
import { Diagnostic } from './diagnostic';

export class Consultation {
    id: number;
    medic: Medic;
    patient: Patient;
    service: Service;
    dateTime: Date;
    symptoms: string;
    prescription: string;
    sickLeaveStart: Date;
    sickLeaveEnd: Date;
    diagnostic: Diagnostic;

    // constructor(id: number, medic: Medic, patient: Patient, service: Service, date: Date, symptoms: string,
    //      prescription: string, sickLeaveStart: Date, sickLeaveEnd: Date, diagnostic: Diagnostic ){
    //         this.id = id;
    //         this.medic = medic;
    //         this.service = service;
    //         this.date = date;
    //         this.symptoms = symptoms;
    //         this.prescription = prescription;
    //         this.sickLeaveStart = sickLeaveStart;
    //         this.sickLeaveEnd = sickLeaveEnd;
    //         this.diagnostic = diagnostic;
    //      }

    constructor(patient: Patient, medic: Medic, service: Service, date: Date) {
        this.patient = patient;
        this.medic = medic;
        this.service = service;
        this.dateTime = date;
    }

}
