export class MedicConsultation {
    fullName: string;
    nrOfConsultations: number;

    constructor( fullName: string, nrOfConsultations: number) {
        this.fullName = fullName;
        this.nrOfConsultations = nrOfConsultations;
    }
}
