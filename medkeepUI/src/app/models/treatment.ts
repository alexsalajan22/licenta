export class Treatment {

    idTreatment: number;
    name: string;
    price: number;
    description: string;
    category: string;
    // medicList: Medic[];
    // serviceList: Service[];

    constructor(id: number, name: string, price: number, description: string, category: string) {
        this.idTreatment = id;
        this.name = name;
        this.price = price;
        this.description = description;
        this.category = category;
    }
}
