export class DepartmentMedicConsultations {
    departmentName: string;
    medicNameList: string[];
    nrConsultationsList: number[];

   constructor(departmentName: string, serviceNameList: string[], nrConsultationsList: number[]) {
    this.departmentName = departmentName;
    this.medicNameList = serviceNameList;
    this.nrConsultationsList = nrConsultationsList;
   }
}
