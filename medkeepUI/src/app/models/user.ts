import { Department } from './department';

export class User {
    idUser: number;
    firstName: string;
    lastName: string;
    birthDate: Date;
    email: string;
    phoneNumber: string;
    userType: string;
    city: string;
    adress: string;

    constructor(id: number, firstName: string, lastName: string, date: Date,
        email: string, phoneNr: string, userType: string) {
            this.idUser = id;
            this.firstName = firstName;
            this.lastName = lastName;
            this.birthDate = date;
            this.email = email;
            this.phoneNumber = phoneNr;
            this.userType = userType;
        }
}

export class Medic extends User {
    password: string;
    specialization: string;
    description: string;
    department: Department;


    constructor (id: number, firstName: string, lastName: string, date: Date,
        email: string, phoneNr: string, password: string, specialization: string, description: string, department: Department) {
            super(id, firstName, lastName, date, email, phoneNr, 'MEDIC');
            this.password = password;
            this.specialization = specialization;
            this.description = description;
            this.department = department;
        }
}

export class Patient extends User {
    cnp: string;
    adress: string;
    city: string;
    // conslultationList: Consultation[];

    constructor(id: number, firstName: string, lastName: string, date: Date,
        email: string, phoneNr: string, cnp: string, adress: string, city: string) {
            super(id, firstName, lastName, date, email, phoneNr, 'PATIENT');
            this.cnp = cnp;
            this.adress = adress;
            this.city = city;
        }
}

export class Receptionist extends User {
    password: string;
    adress: string;

    constructor(id: number, firstName: string, lastName: string, date: Date,
        email: string, phoneNr: string, password: string, adress: string) {
            super(id, firstName, lastName, date, email, phoneNr, 'RECEPTIONIST');
            this.password = password;
            this.adress = adress;
        }
}

export class Administrator extends User {
    password: string;

    constructor(id: number, firstName: string, lastName: string, date: Date,
        email: string, phoneNr: string, password: string, adress: string) {
            super(id, firstName, lastName, date, email, phoneNr, 'ADMIN');
            this.password = password;
    }
}
