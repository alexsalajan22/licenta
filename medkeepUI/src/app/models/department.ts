export class Department {
    idDepartment: number;
    name: string;
    description: string;
    // medicList: Medic[];
    // serviceList: Service[];

    constructor(id: number, name: string, description: string) {
        this.idDepartment = id;
        this.name = name;
        this.description = description;
    }
}
