export class Mail {
    subject: string;
    receiver: string;
    body: string;


    constructor( subject: string, receiver: string, body: string ) {
        this.subject = subject;
        this.receiver = receiver;
        this.body = body;
    }
}
