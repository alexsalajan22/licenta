import {Department} from './department';

export class Service {
    idService: number;
    name: string;
    description: string;
    price: number;
    department: Department;

    constructor(id: number, name: string, description: string, price: number, department: Department) {
        this.idService = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.department = department;
    }
}
